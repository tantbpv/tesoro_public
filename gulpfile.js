var gulp = require('gulp'),
		autoprefixer = require('gulp-autoprefixer'),
		concatCss = require('gulp-concat-css'),
		minifyCSS = require('gulp-minify-css'),
		less = require('gulp-less'),
		sourcemaps = require('gulp-sourcemaps'),
		plumber = require('gulp-plumber'),
		uglify = require('gulp-uglify'),
		notify = require("gulp-notify"),
		watch = require('gulp-watch'),
		rename = require("gulp-rename"),
		livereload = require('gulp-livereload'),
		connect = require('gulp-connect'),
		minifyHTML = require('gulp-minify-html'),
		sass = require('gulp-sass'),
		jshint = require('gulp-jshint'),
		fileinclude = require('gulp-file-include');
		var concat = require('gulp-concat');
//csslint = require('gulp-csslint');


var opts = {
	conditionals: true,
	spare: true
};
var path = {
	build: {
		html: 'build/',
		js: 'build/js/',
		css: 'build/css/',
		img: 'build/img/',
		fonts: 'build/fonts/',
		libs: 'build/libs/'
	},
	src: {
		html: 'src/*.*',
		js: 'src/js/*.js',
		style: 'src/style/*.scss',
		img: 'src/img/**/*.*',
		fonts: 'src/fonts/**/*.*',
		libs: 'src/libs/**/*.*'
	},
	watch: {
		html: 'src/**/*.html',
		js: 'src/js/**/*.js',
		style: 'src/style/**/*.scss',
		img: 'src/img/**/*.*',
		fonts: 'src/fonts/**/*.*',
		libs: 'src/libs/**/*.*'
	},

};

gulp.task('lint', function () {
	return gulp.src('src/js/**/*.js')
			.pipe(jshint())
			.pipe(jshint.reporter('default'));
});
/*
 gulp.task('csslint', function() {
 gulp.src(path.build.style)
 .pipe(csslint())
 .pipe(csslint.reporter());
 });
 */
gulp.task('connect', function () {
	connect.server({
		root: 'build',
		port: 8888,
		livereload: true
	});
});

gulp.task('html:build', function () {
	gulp.src(path.src.html)
	//.pipe(minifyHTML(opts))
			.pipe(plumber())
			.pipe(fileinclude({
				prefix: '@@',
				basepath: '@file'
			}))
			.pipe(connect.reload())
			.pipe(gulp.dest(path.build.html))
			.pipe(notify("html ready!"));
});

gulp.task('js:build', function () {
	gulp.src(path.src.js)
			.pipe(sourcemaps.init())
			.pipe(plumber())
			.pipe(concat('main.js'))
			//.pipe(uglify())
			.pipe(sourcemaps.write())
			.pipe(gulp.dest(path.build.js))
			.pipe(connect.reload())
			.pipe(notify("js ready!"));
});

gulp.task('style:build', function () {
	gulp.src(path.src.style)
			.pipe(sourcemaps.init())
			.pipe(plumber())
			//.pipe(less())
			.pipe(sass().on('error', sass.logError))
			.pipe(autoprefixer())
			//.pipe(minifyCSS())
			//.pipe(sourcemaps.write())
			.pipe(gulp.dest(path.build.css))
			.pipe(connect.reload())
			.pipe(notify("css ready!"));
});

gulp.task('image:build', function () {
	gulp.src(path.src.img)
			.pipe(gulp.dest(path.build.img));
			//.pipe(notify("images ready!"));
});

gulp.task('fonts:build', function () {
	gulp.src(path.src.fonts)
			.pipe(gulp.dest(path.build.fonts));
			//.pipe(notify("fonts ready!"));
});
gulp.task('libs:build', function () {
	gulp.src(path.src.libs)
			.pipe(gulp.dest(path.build.libs));
			//.pipe(notify("libs ready!"));
});


gulp.task('build', [
	'html:build',
	'js:build',
	'style:build',
	'fonts:build',
	'image:build',
	'libs:build'
]);

gulp.task('watch', function () {
	//livereload.listen();
	watch([path.watch.html], function (event, cb) {
		gulp.start('html:build');
	});
	watch([path.watch.style], function (event, cb) {
		gulp.start('style:build');
		//gulp.start('csslint');
	});
	watch([path.watch.js], function (event, cb) {
		gulp.start('js:build');
		gulp.start('lint');
	});
	/*watch([path.watch.img], function (event, cb) {
		gulp.start('image:build');
	});
	watch([path.watch.fonts], function (event, cb) {
		gulp.start('fonts:build');
	});
	watch([path.watch.libs], function (event, cb) {
		gulp.start('libs:build');
	});*/
});

gulp.task('default', ['build', 'connect', 'watch', 'lint']);