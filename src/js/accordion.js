$(function () {
	var $accrodion = $(".js-accordion");
	var closeClass = "is-close";
	var speed = 200;
	var isClose = false;

	function openAccordion() {
		$(this).removeClass(closeClass);
		$(this).find(".accordion__content").slideDown(speed);
	}
	function closeAccordion() {
		$(this).addClass(closeClass);
		$(this).find(".accordion__content").slideUp(speed);
	}

	if($accrodion.length) {
		$accrodion.each(function (i, el) {
			var btn = $(el).find(".accrodion__btn");

			if(!isClose) {
				closeAccordion.call(el);
			}

			btn.on("click", function () {
				console.log("click");
				if($(el).hasClass(closeClass)) {
					openAccordion.call(el);
				} else {
					closeAccordion.call(el);
				}
			});
		});// end .each()
	}
});