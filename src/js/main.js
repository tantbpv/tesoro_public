$(document).ready(function () {
	var arrowLeft = '<i class="icon-arrow_left"></i>';
	var arrowRight = '<i class="icon-arrow_right"></i>';

	var bannerSlider = $('.js-banner-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		infinite: true,
		centerMode: true,
		centerPadding: '0',
		//variableWidth: true,
		lazyLoad: 'ondemand',
		nextArrow: '<button type="button" class="banner-slider__btn--next banner-slider__btn"> ' + arrowRight + ' </button>',
		prevArrow: '<button type="button" class="banner-slider__btn--prev banner-slider__btn">' + arrowLeft + '</button>',
		swipeToSlide: true
	});
	$(window).resize(function () {
		bannerSlider.slick('slickGoTo', 0);
	});
	bannerSlider.slick('slickGoTo', 0);

	// cart module start
	var $cart = $(".js-cart");
	var $cartLabel = $(".js-cart-open-close-label");

	function openCart() {
		$cart.removeClass("cart--close");
		$cart.addClass("cart--open");
	}

	function closeCart() {
		$cart.removeClass("cart--open");
		$cart.addClass("cart--close");
	}

	function checkCartPosition() {
		var bottomEdge = $(".footer").offset().top;
		var cartPositionBottom = $(window).scrollTop() + $(window).height();
		var scrollPositionBotom;
		if (bottomEdge < cartPositionBottom) {
			$cart.css({
				"bottom": cartPositionBottom - bottomEdge,
				"opacity": 0,
				"pointer-events": "none"
			});
		} else {
			$cart.css({
				"bottom": 0,
				"opacity": 1,
				"pointer-events": "auto"
			});
		}
	}
	checkCartPosition();
	// cart listeners 
	$cartLabel.on("click", function () {
		if ($cart.hasClass("cart--close")) {
			openCart();
		} else if ($cart.hasClass("cart--open")) {
			closeCart();
		} else {
			console.error("cart error in class!");
		}
	});
	$cart.on("click", function (e) {
		e.stopPropagation();
	});
	$(".wrapper").on("click", function (e) {
		e.stopPropagation();
		closeCart();
	});

	/*
	$(window).on("scroll", function(e) {
		closeCart();
	}); */
	$(window).on("resize", function (e) {
		closeCart();
		checkCartPosition();
	});

	$(window).on("scroll", function () {
		checkCartPosition();
	});
	// cart listeners end
	// custom scrollbar in cart
	$(window).load(function () {
		$(".js-custom-scrollbar").mCustomScrollbar({
			setHeight: "100%"
		});
	});

	//cart module end

	/* smooth scrolling */
	(function () {
		var target = $('a[href^="#"]');

		function smoothScroll() {
			// add "smooth_scroll" class to the link with href="#destination"
			elementClick = $(this).attr("href");
			destination = $(elementClick).offset().top;
			$('body').animate({
				scrollTop: destination
			}, 1100);
			$('html').animate({
				scrollTop: destination
			}, 1100);
			return false;
		}
		target.on("click", smoothScroll);
	})();
	/* smooth scrolling end*/

	// category menu 
	var mainMenuBtn = $(".category-menu__mobile-title");
	var subMenuBtn = $(".category-menu__item > i");
	var menuBody = $(".category-menu__list");

	function showCatMenu() {
		menuBody.slideDown(200);
		mainMenuBtn.find(" > i ").removeClass("fa-plus").addClass("fa-minus");
	}

	function hideCatMenu() {
		menuBody.slideUp(200);
		mainMenuBtn.find(" > i ").removeClass("fa-minus").addClass("fa-plus");
	}

	function showSubmenu(menuBtn) {
		menuBtn.parent().find(" > ul ").slideDown(200);
		menuBtn.removeClass("fa-plus").addClass("fa-minus");
	}

	function hideSubmenu(menuBtn) {
		menuBtn.parent().find(" > ul ").slideUp(200);
		menuBtn.removeClass("fa-minus").addClass("fa-plus");
	}

	$(window).resize(function () {
		var mql_768 = window.matchMedia("(max-width: 767px)").matches;
		//console.log(mql_768);
		if (!mql_768) {
			showCatMenu();
			return;
		} else {
			hideCatMenu();
			return;
		}
	});
	mainMenuBtn.on("click", function () {
		if ($(this).find(" > i ").hasClass("fa-plus")) {
			showCatMenu();
			return;
		} else {
			hideCatMenu();
			return;
		}
	});
	subMenuBtn.on("click", function () {
		if ($(this).hasClass("fa-minus")) {
			hideSubmenu($(this));
			return;
		} else {
			hideSubmenu(subMenuBtn);
			showSubmenu($(this));
		}
	});
	// category menu end


	// footer menu
	$(".footer__down-arrow").on("click", function () {
		$(".footer-menu").toggleClass("footer-menu--open");
	});
	// footer menu end

	// category-slider
	var categorySlider = $('.js-category-slider').slick({
		slidesToShow: 6,
		slidesToScroll: 1,
		arrows: true,
		dots: true,
		infinite: true,
		lazyLoad: 'ondemand',
		nextArrow: '<button type="button" class="category-slider__btn--next category-slider__btn">' + arrowRight + '</button>',
		prevArrow: '<button type="button" class="category-slider__btn--prev category-slider__btn">' + arrowLeft + '</button>',
		swipeToSlide: true,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 3
				}
    },
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1
				}
    }
  ]
	});
	// category-slider end

	// only mumbers input
	$("#amount__min, #amount__max, .js-only-numbers").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
			// Allow: Ctrl+A, Command+A
			(e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow: home, end, left, right, down, up
			(e.keyCode >= 35 && e.keyCode <= 40)) {
			// let it happen, don't do anything
			return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});
	// only mumbers input end

	// fillter price slider
	(function () {

		var sliderMinRange = 500;
		var sliderMaxRange = 20000;
		$(function () {
			$("#slider-range").slider({
				range: true,
				min: sliderMinRange,
				max: sliderMaxRange,
				values: [sliderMinRange, sliderMaxRange],
				slide: function (event, ui) {
					$("#amount__min").val(ui.values[0]);
					$("#amount__max").val(ui.values[1]);
					//console.log(ui.values[0]);
					//console.log(ui.values[1]);
				}
			});

			$("#amount__min, #amount__max").change(function () {
				var valueMin = parseInt($("#amount__min").val());
				var valueMax = parseInt($("#amount__max").val());
				//console.log("min " + valueMin);
				//console.log("max " + valueMax);
				if (valueMin > valueMax) {
					valueMin = valueMax;
					//console.log("valueMin = valueMax")
				}
				if (valueMin < sliderMinRange) {
					valueMin = sliderMinRange;
					//console.log("valueMin = 500;")
				}
				if (valueMax > sliderMaxRange) {
					valueMax = sliderMaxRange;
					//console.log("valueMax = 20000;")
				}
				//console.log(valueMin);
				//console.log(valueMax);
				//console.log("slider values " + $( "#slider-range" ).slider( "option", "values" ));

				// set slider values = input values
				$("#slider-range").slider("option", "values", [valueMin, valueMax]);

				// set input values = slider values

				$("#amount__min").val(valueMin);
				$("#amount__max").val(valueMax);
			});


		});
	})();
	// fillter price slider end 


	// js-product-grid-slider
	var productGridSlider = $('.js-product-grid-slider').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: true,
		dots: false,
		infinite: true,
		lazyLoad: 'ondemand',
		nextArrow: '<button type="button" class="product-grid-slider__btn--next product-grid-slider__btn">' + arrowRight + '</button>',
		prevArrow: '<button type="button" class="product-grid-slider__btn--prev product-grid-slider__btn">' + arrowLeft + '</button>',
		swipeToSlide: true,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 2
				}
    },
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1
				}
    }
  ]
	});
	// js-product-grid-slider end

	// js-img-slider
	var imgSlider = $('.js-img-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		dots: false,
		infinite: true,
		lazyLoad: 'ondemand',
		nextArrow: '<button type="button" class="slider__btn--next slider__btn">' + arrowRight + '</button>',
		prevArrow: '<button type="button" class="slider__btn--prev slider__btn">' + arrowLeft + '</button>',
		swipeToSlide: true
	});
	// js-img-slider end

	// js-brand-slider
	var brandSlider = $('.js-brand-slider');

			brandSlider.slick({
				slidesToShow: 5,
				slidesToScroll: 1,
				rows: 2,
				arrows: true,
				dots: false,
				infinite: true,
				lazyLoad: 'ondemand',
				nextArrow: '<button type="button" class="slider__btn--next slider__btn">' + arrowRight + '</button>',
				prevArrow: '<button type="button" class="slider__btn--prev slider__btn">' + arrowLeft + '</button>',
				swipeToSlide: true,
				responsive: [
					{
						breakpoint: 992,
						settings: {
							slidesToShow: 2
						}
		    },
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 1
						}
		    }
		  ]
		});


		
	// js-brand-slider end
	
	// js-prod-slider
	var $gridSliders = $(".js-grid-slider");

	if($gridSliders.length) {
		$gridSliders.each(function (i, el) {
			var $gallerySlider = $(el).find(".js-prod-gallery-slider");
			var $imagesSlider = $(el).find(".js-prod-image-slider");
			var $gallerySliderIndex = $gallerySlider.addClass("js-prod-gallery-slider--" + i);
			var $imagesSliderIndex = $imagesSlider.addClass("js-prod-image-slider--" + i);
			//console.log($gallerySliderIndex);
			//console.log($imagesSliderIndex);
			initGridSlider($gallerySliderIndex, $imagesSliderIndex);
		});// end .each()
	}

	function initGridSlider($gallerySliderIndex, $imagesSliderIndex) {
		var $gallerySlider = $gallerySliderIndex;
		$gallerySlider.slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			arrows: false,
			dots: false,
			lazyLoad: 'ondemand',
			vertical: true,
			infinite: true,
			verticalSwiping: true,
			swipeToSlide: true,
			focusOnSelect: true,
			asNavFor: $imagesSliderIndex,
			responsive: [

				{
					breakpoint: 768,
					settings: {
						vertical: false
					}
    }
  ]
		});
		var $imagesSlider = $imagesSliderIndex;
		$imagesSlider.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			dots: false,
			fade: true,
			infinite: true,
			lazyLoad: 'ondemand',
			asNavFor: $gallerySliderIndex,
			swipe: true,
			swipeToSlide: false
		});
		//console.log("initProdSlider()");
	}
	// js-prod-slider end

	// jquery ui tabs
	$(".js-ui-tabs").tabs({
		beforeActivate: function (event, ui) {
			productGridSlider.slick("slickGoTo", 0); // fix for slick slider
		}
	});
	// jquery ui tabs end

	function initForms() {
		// tsr-select
		var select = $(".tsr-select");
		select.styler();
		// tsr-select end

		// tsr-quantity-forms increment
		function initQuantityForm() {
			var input = $(".tsr-quantity-forms > input");

			if (input.hasClass("quantity-forms-init")) {
				return;
			} else {
				$(".tsr-quantity-forms__inc, .tsr-quantity-forms__dec").on("click", function () {
					var $button = $(this);
					var oldValue = $button.parent().find("input").val();
					if ($button.hasClass("tsr-quantity-forms__inc")) {
						var newVal = parseFloat(oldValue) + 1;
					} else if ($button.hasClass("tsr-quantity-forms__dec")) {
						if (oldValue > 1) {
							var newVal = parseFloat(oldValue) - 1;
						} else {
							newVal = 1;
						}
					}
					$button.parent().find("input").val(newVal).trigger("change");
				});
				input.addClass("quantity-forms-init");
			}

		}
		initQuantityForm();
		// tsr-quantity-forms end	

		// tsr-rating-input
		$('.js-rating-input').rating();
		// tsr-rating-input end
	}

	initForms();




	// file input controller
	(function () {
		//var file_api = ( window.File && window.FileReader && window.FileList && window.Blob ) ? true : false;
		var fileInput = $('#file_btn');
		var fileInputLabel = $('.file-input__label');

		var openFile = function (event) {
			var input = event.target;
			if (input.files[0].type.match('image.*')) {
				//console.log("img");
				var reader = new FileReader();
				var dataURL;
				reader.onload = function () {
					dataURL = reader.result;
					fileInputLabel.css("background-image", "url(" + dataURL + ")");
				};
				reader.readAsDataURL(input.files[0]);
			} else {
				alert("Можно загружать только фото");
			}

		};
		fileInput.on("change", function (e) {
			openFile(e);
		});
	}());
	// file input controller end

	// pop-up init
	$(".js-popup").magnificPopup({
		type: 'inline',
		preloader: false,
		focus: false
	});

	$('.js-popup-video').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,

		fixedContentPos: false
	});
	$(".js-fast-view").magnificPopup({
		type: 'ajax',
		preloader: true,
		focus: false,
		showCloseBtn: false,
		callbacks: {
			ajaxContentAdded: function () {
				initProdSlider();
				initForms();
			}
		}
	});
	
	$(".js-pop-up-ajax").magnificPopup({
		type: 'ajax',
		preloader: true,
		focus: false,
		showCloseBtn: false,
		callbacks: {
			ajaxContentAdded: function () {
				initForms();
			}
		}
	});
	// pop-up init end

	// filter show script
	var filterBtn = $(".js-filter__title");
	var filterBody = $(".js-filter__inner");
	filterBtn.on("click", function () {
		filterBody.slideToggle();
	});
	$(window).resize(function () {
		var mql_992 = window.matchMedia("(max-width: 992px)").matches;
		//console.log(mql_768);
		if (!mql_992) {
			filterBody.attr("style", "");
		}
	});
	// filter show script end



}); // ready

// registration-success
function showRegSuccess() {
	$.magnificPopup.open({
		items: {
			src: '#registration-success'
		},
		type: 'inline',
		preloader: false,
		focus: false
	});
}
// registration-success end

$(document).ready(function () {
	var arrowLeft = '<i class="icon-arrow_left"></i>';
	var arrowRight = '<i class="icon-arrow_right"></i>';

	var	$ourBrendsSliderVideo = $(".js-our-brends-slider-video"),
			ourSlideVideo = $ourBrendsSliderVideo.find(".banner"),
			ourBrendsSliderHalf = "our-brends-slider";

	function styleBanner () {
		$ourBrendsSliderVideo.addClass(ourBrendsSliderHalf);
	}

	function initSlider () {
		$ourBrendsSliderVideo.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			infinite: true,
			centerMode: true,
			centerPadding: '290px',
			//variableWidth: true,
			lazyLoad: 'ondemand',
			nextArrow: '<button type="button" class="banner-slider__btn--next banner-slider__btn"> ' + arrowRight + ' </button>',
			prevArrow: '<button type="button" class="banner-slider__btn--prev banner-slider__btn">' + arrowLeft + '</button>',
			swipeToSlide: true,
			responsive: [
		    {
		      breakpoint: 1600,
		      settings: {
		      	centerPadding: "100px"
		      }
		    },
		    {
		      breakpoint: 1024,
		      settings: {
		      	slidesToShow: 1,
						slidesToScroll: 1,
						centerPadding: "100px"
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						centerPadding: "50px"
		      }
		    }
		  ]
		});

	$(window).resize(function () {
		$ourBrendsSliderVideo.slick('slickGoTo', 0);
	});

		$ourBrendsSliderVideo.slick('slickGoTo', 0);
	}


	function initVideo () {
		$ourBrendsSliderVideo.removeClass('our-brends-slider-full').removeClass(ourBrendsSliderHalf);
		if(ourSlideVideo.length === 1) {
			$ourBrendsSliderVideo.addClass("our-brends-slider-full");
		} else if (ourSlideVideo.length === 2) {
			styleBanner ();
		} else {
			initSlider ();
		}
	}

	var $ourBrendsSlider = $(".js-our-brands-slider"),
			$ourBrendsSliderTest = $(".js-our-brands-slider-test"),
			$ourBrendsSliderSlide = $ourBrendsSlider.find(".banner");
			var slideToShow = 2;

		if($ourBrendsSliderSlide.length === 2) {
			slideToShow = 1;
			initOurBrandsSlider();
		}

		if($ourBrendsSliderSlide.length > 2) {
			initOurBrandsSlider();
		}

	function initOurBrandsSlider () {
		$ourBrendsSlider.slick({
		  infinite: true,
		  speed: 300,
		  slidesToShow: slideToShow,
		  slidesToScroll: 1,
		  centerMode: true,
		  centerPadding: '150px',
	    nextArrow: '<button type="button" class="banner-slider__btn--next banner-slider__btn"> ' + arrowRight + ' </button>',
			prevArrow: '<button type="button" class="banner-slider__btn--prev banner-slider__btn">' + arrowLeft + '</button>',
	    variableWidth: true,
	    responsive: [{
	        breakpoint: 992,
	        settings: {
	        	centerPadding: '50px',
	        }
	    },{
	        breakpoint: 768,
	        settings: {
	            slidesToShow: 1,
	            slidesToScroll: 1,
	            centerPadding: '0px',
	        }
	    }]
	});

		$(window).resize(function () {
			$ourBrendsSlider.slick('slickGoTo', 0);
		});

		$ourBrendsSlider.slick('slickGoTo', 0);
	}

		

	$ourBrendsSliderTest.slick({
	  infinite: true,
	  speed: 300,
	  slidesToShow: 2,
	  slidesToScroll: 1,
	  centerMode: true,
	  centerPadding: '150px',
    nextArrow: '<button type="button" class="banner-slider__btn--next banner-slider__btn"> ' + arrowRight + ' </button>',
		prevArrow: '<button type="button" class="banner-slider__btn--prev banner-slider__btn">' + arrowLeft + '</button>',
    //variableWidth: true,
     responsive: [{
	        breakpoint: 992,
	        settings: {
	        	centerPadding: '100px',
	        }
	    },{
	        breakpoint: 768,
	        settings: {
	            slidesToShow: 1,
	            slidesToScroll: 1,
	            centerPadding: '50px',
	        }
	    }]
	});

		$(window).resize(function () {
			$ourBrendsSliderTest.slick('slickGoTo', 0);
		});

		$ourBrendsSliderTest.slick('slickGoTo', 0);

	initVideo ();


});
//our-brends-slider 
