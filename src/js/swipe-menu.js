$(function () {
	// swipe menu
	(function () {
		var $touch = $('.js-toggle-menu');
		var $touch_animate = $('.js-toggle-menu .sandwich');
		var className = "active";
		var $menu = $('.swipe-menu');

		function showMenu() {
			$("body").addClass(className);
			$touch_animate.addClass(className);
			//console.log("show");
		}

		function hideMenu() {
			$("body").removeClass(className);
			$touch_animate.removeClass(className);
			//console.log("hide");
		}

		$touch.on('click', function (e) {
			e.preventDefault();
			e.stopPropagation();
			//console.log("click");
			//console.log(!($touch_animate.hasClass(className)));
			if (!($touch_animate.hasClass(className))) {
				showMenu();
			} else {
				hideMenu();
			}
		});
		$(".wrapper").on('click', function (e) {
			//e.preventDefault();
			e.stopPropagation();
			hideMenu();
		});
		$menu.on('click', function (e) {
			e.stopPropagation();
		});
		$(window).resize(function () {
			var wid = $(window).width();
			if (wid > 767) {
				hideMenu();
			}
		});
	})();
	// swipe menu end
});