$(function () {
	var $accrodion = $(".js-accordion");
	var closeClass = "is-close";
	var speed = 200;
	var isClose = false;

	function openAccordion() {
		$(this).removeClass(closeClass);
		$(this).find(".accordion__content").slideDown(speed);
	}
	function closeAccordion() {
		$(this).addClass(closeClass);
		$(this).find(".accordion__content").slideUp(speed);
	}

	if($accrodion.length) {
		$accrodion.each(function (i, el) {
			var btn = $(el).find(".accrodion__btn");

			if(!isClose) {
				closeAccordion.call(el);
			}

			btn.on("click", function () {
				console.log("click");
				if($(el).hasClass(closeClass)) {
					openAccordion.call(el);
				} else {
					closeAccordion.call(el);
				}
			});
		});// end .each()
	}
});
// google map api
//AIzaSyDHHUnlsnx3cOKdUiz3cmH8z0V2VLOD-oA
//
var svgCloseIcon = '<svg enable-background="new 0 0 512 512" id="Layer_1" version="1.1" viewBox="0 0 512 512" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="512" height="512"> <g>	<path d="M501.5,512c-2.7,0-5.4-1-7.5-3.1L3.1,18C-1,13.9-1,7.2,3.1,3.1C7.2-1,13.9-1,18,3.1L508.9,494   c4.1,4.1,4.1,10.8,0,14.9C506.9,511,504.2,512,501.5,512z" fill="#6A6E7C" /><path d="M10.5,512c-2.7,0-5.4-1-7.5-3.1c-4.1-4.1-4.1-10.8,0-14.9L494,3.1c4.1-4.1,10.8-4.1,14.9,0   c4.1,4.1,4.1,10.8,0,14.9L18,508.9C15.9,511,13.2,512,10.5,512z" fill="#6A6E7C" /></g></svg>';
var markersInstance = {};

function closeInfoWindow() {
	if (infoWindow) {
		infoWindow.close();
		for (var markerKey in markersInstance) {
			markersInstance[markerKey].setIcon("img/icon/marker.png");
		}
	}
}

function  customizeInfoWindow(infoWindow) {
	// *
	// START INFOWINDOW CUSTOMIZE.
	// The google.maps.event.addListener() event expects
	// the creation of the infowindow HTML structure 'domready'
	// and before the opening of the infowindow, defined styles are applied.
	// *
	if (infoWindow) {

		google.maps.event.addListener(infoWindow, 'domready', function () {

			// Reference to the DIV that wraps the bottom of infowindow
			var iwOuter = $('.gm-style-iw');

			/* Since this div is in a position prior to .gm-div style-iw.
			 * We use jQuery and create a iwBackground variable,
			 * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
			 */
			var iwBackground = iwOuter.prev();

			// Removes background shadow DIV
			iwBackground.children(':nth-child(2)').css({'display': 'none'});

			// Removes white background DIV
			iwBackground.children(':nth-child(4)').css({'display': 'none'});

			// Moves the infowindow 115px to the right.
			//iwOuter.parent().parent().css({left: '0px'});

			// Moves the shadow of the bottom arrow.
			iwBackground.children(':nth-child(1)').css('display', "none");
			// Remove the bottom arrow.

			iwBackground.children(':nth-child(3)').css('display', "none");

			// Changes the desired tail shadow color.
			/*iwBackground.children(':nth-child(3)').find('div').children().css({
				'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px',
				'z-index': '1'
			});*/

			// Reference to the div that groups the close button elements.
			var iwCloseBtn = iwOuter.next();
			// hide close btn
			iwCloseBtn.css('display', "none");
			// Apply the desired effect to the close button
			// iwCloseBtn.css({
			// 	opacity: '1',
			// 	right: '10px',
			// 	top: '10px',
			// 	border: 'none'
			// });

			// If the content of infowindow not exceed the set maximum height, then the gradient is removed.
			/*if ($('.iw-content').height() < 140) {
				$('.iw-bottom-gradient').css({display: 'none'});
			}*/

			// The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
			iwCloseBtn.mouseout(function () {
				$(this).css({opacity: '1'});
			});
		});
	}
}

var infoWindow = null;
function addInfoWindow(marker, message) {

	// show infoWindow
	google.maps.event.addListener(marker, 'click', function () {
		closeInfoWindow();

		infoWindow = new google.maps.InfoWindow({
			content: '<div class="ballon"><span class="ballon__close" onclick="closeInfoWindow();">' + svgCloseIcon + '</span>' + message + '</div>',
			maxWidth: 400
		});
		// set custom styles
		customizeInfoWindow(infoWindow);
		// hide marker
		marker.setIcon("img/icon/marker-empty.png");

		// waiting styles
		setTimeout(function () {
			infoWindow.open(gMap, marker);
		}, 70);

	});

}//addInfoWindow

function initMap(mapConfig) {

	// Specify features and elements to define styles.
	var styleArray = [
		{
			featureType: "all",
			stylers: [
				{
					saturation: 0
				}
			]
		}, {
			featureType: "poi.business",
			//elementType: "labels",
			stylers: [
				{
					visibility: "off"
				}
			]
		}
	];

	var isDraggable = $(document).width() > 768 ? true : false; // If document (your website) is wider than 768px, isDraggable = true, else isDraggable = false

	var mapOptions = {
		draggable: isDraggable,
		center: {
			lat: mapConfig.mapCenter[0],
			lng: mapConfig.mapCenter[1]
		},
		// Apply the map style array to the map.
		styles: styleArray,
		zoom: mapConfig.zoom,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		panControl: false,
		zoomControl: true,
		mapTypeControl: false,
		scaleControl: false,
		streetViewControl: true,
		overviewMapControl: false,
		scrollwheel: false // Prevent users to start zooming the map when scrolling down the page
		//... options options options
	};
	// Create a map object and specify the DOM element for display.
	gMap = new google.maps.Map(document.getElementById('g-map'), mapOptions);

	// Event that closes the Info Window with a click on the map
	google.maps.event.addListener(gMap, 'click', function () {
		closeInfoWindow();
	});

	// create markers
	var markers = mapConfig.markers;
	var markerPosition = {};

	for (var marker in markers) {

		// // create new marker position instance
		markerPosition[marker] = new google.maps.LatLng(markers[marker].center[0], markers[marker].center[1]);
		// create new marker instance
		markersInstance[marker] = new google.maps.Marker({
			position: markerPosition[marker],
			map: gMap,
			title: markers[marker].title,
			icon: "img/icon/marker.png"
		});

		// create and show infoWindow on click
		addInfoWindow(markersInstance[marker], markers[marker].baloonContent);


		//console.log(markersInstance[marker]);
	}


}// initMap

function centeringMarker(latitude, longitude) {
	//centering map
	// because gladiolus
	//gMap.setCenter({lat: latitude, lng: longitude});
	gMap.panTo({lat: latitude, lng: longitude});
	//return false;
}

$(window).on("load", function () {
	if (typeof mapConfig != "undefined") {
		var gMap;
		initMap(mapConfig);
	}
});
// google map api end
$(document).ready(function () {
	var arrowLeft = '<i class="icon-arrow_left"></i>';
	var arrowRight = '<i class="icon-arrow_right"></i>';

	var bannerSlider = $('.js-banner-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		infinite: true,
		centerMode: true,
		centerPadding: '0',
		//variableWidth: true,
		lazyLoad: 'ondemand',
		nextArrow: '<button type="button" class="banner-slider__btn--next banner-slider__btn"> ' + arrowRight + ' </button>',
		prevArrow: '<button type="button" class="banner-slider__btn--prev banner-slider__btn">' + arrowLeft + '</button>',
		swipeToSlide: true
	});
	$(window).resize(function () {
		bannerSlider.slick('slickGoTo', 0);
	});
	bannerSlider.slick('slickGoTo', 0);

	// cart module start
	var $cart = $(".js-cart");
	var $cartLabel = $(".js-cart-open-close-label");

	function openCart() {
		$cart.removeClass("cart--close");
		$cart.addClass("cart--open");
	}

	function closeCart() {
		$cart.removeClass("cart--open");
		$cart.addClass("cart--close");
	}

	function checkCartPosition() {
		var bottomEdge = $(".footer").offset().top;
		var cartPositionBottom = $(window).scrollTop() + $(window).height();
		var scrollPositionBotom;
		if (bottomEdge < cartPositionBottom) {
			$cart.css({
				"bottom": cartPositionBottom - bottomEdge,
				"opacity": 0,
				"pointer-events": "none"
			});
		} else {
			$cart.css({
				"bottom": 0,
				"opacity": 1,
				"pointer-events": "auto"
			});
		}
	}
	checkCartPosition();
	// cart listeners 
	$cartLabel.on("click", function () {
		if ($cart.hasClass("cart--close")) {
			openCart();
		} else if ($cart.hasClass("cart--open")) {
			closeCart();
		} else {
			console.error("cart error in class!");
		}
	});
	$cart.on("click", function (e) {
		e.stopPropagation();
	});
	$(".wrapper").on("click", function (e) {
		e.stopPropagation();
		closeCart();
	});

	/*
	$(window).on("scroll", function(e) {
		closeCart();
	}); */
	$(window).on("resize", function (e) {
		closeCart();
		checkCartPosition();
	});

	$(window).on("scroll", function () {
		checkCartPosition();
	});
	// cart listeners end
	// custom scrollbar in cart
	$(window).load(function () {
		$(".js-custom-scrollbar").mCustomScrollbar({
			setHeight: "100%"
		});
	});

	//cart module end

	/* smooth scrolling */
	(function () {
		var target = $('a[href^="#"]');

		function smoothScroll() {
			// add "smooth_scroll" class to the link with href="#destination"
			elementClick = $(this).attr("href");
			destination = $(elementClick).offset().top;
			$('body').animate({
				scrollTop: destination
			}, 1100);
			$('html').animate({
				scrollTop: destination
			}, 1100);
			return false;
		}
		target.on("click", smoothScroll);
	})();
	/* smooth scrolling end*/

	// category menu 
	var mainMenuBtn = $(".category-menu__mobile-title");
	var subMenuBtn = $(".category-menu__item > i");
	var menuBody = $(".category-menu__list");

	function showCatMenu() {
		menuBody.slideDown(200);
		mainMenuBtn.find(" > i ").removeClass("fa-plus").addClass("fa-minus");
	}

	function hideCatMenu() {
		menuBody.slideUp(200);
		mainMenuBtn.find(" > i ").removeClass("fa-minus").addClass("fa-plus");
	}

	function showSubmenu(menuBtn) {
		menuBtn.parent().find(" > ul ").slideDown(200);
		menuBtn.removeClass("fa-plus").addClass("fa-minus");
	}

	function hideSubmenu(menuBtn) {
		menuBtn.parent().find(" > ul ").slideUp(200);
		menuBtn.removeClass("fa-minus").addClass("fa-plus");
	}

	$(window).resize(function () {
		var mql_768 = window.matchMedia("(max-width: 767px)").matches;
		//console.log(mql_768);
		if (!mql_768) {
			showCatMenu();
			return;
		} else {
			hideCatMenu();
			return;
		}
	});
	mainMenuBtn.on("click", function () {
		if ($(this).find(" > i ").hasClass("fa-plus")) {
			showCatMenu();
			return;
		} else {
			hideCatMenu();
			return;
		}
	});
	subMenuBtn.on("click", function () {
		if ($(this).hasClass("fa-minus")) {
			hideSubmenu($(this));
			return;
		} else {
			hideSubmenu(subMenuBtn);
			showSubmenu($(this));
		}
	});
	// category menu end


	// footer menu
	$(".footer__down-arrow").on("click", function () {
		$(".footer-menu").toggleClass("footer-menu--open");
	});
	// footer menu end

	// category-slider
	var categorySlider = $('.js-category-slider').slick({
		slidesToShow: 6,
		slidesToScroll: 1,
		arrows: true,
		dots: true,
		infinite: true,
		lazyLoad: 'ondemand',
		nextArrow: '<button type="button" class="category-slider__btn--next category-slider__btn">' + arrowRight + '</button>',
		prevArrow: '<button type="button" class="category-slider__btn--prev category-slider__btn">' + arrowLeft + '</button>',
		swipeToSlide: true,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 3
				}
    },
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1
				}
    }
  ]
	});
	// category-slider end

	// only mumbers input
	$("#amount__min, #amount__max, .js-only-numbers").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
			// Allow: Ctrl+A, Command+A
			(e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow: home, end, left, right, down, up
			(e.keyCode >= 35 && e.keyCode <= 40)) {
			// let it happen, don't do anything
			return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});
	// only mumbers input end

	// fillter price slider
	(function () {

		var sliderMinRange = 500;
		var sliderMaxRange = 20000;
		$(function () {
			$("#slider-range").slider({
				range: true,
				min: sliderMinRange,
				max: sliderMaxRange,
				values: [sliderMinRange, sliderMaxRange],
				slide: function (event, ui) {
					$("#amount__min").val(ui.values[0]);
					$("#amount__max").val(ui.values[1]);
					//console.log(ui.values[0]);
					//console.log(ui.values[1]);
				}
			});

			$("#amount__min, #amount__max").change(function () {
				var valueMin = parseInt($("#amount__min").val());
				var valueMax = parseInt($("#amount__max").val());
				//console.log("min " + valueMin);
				//console.log("max " + valueMax);
				if (valueMin > valueMax) {
					valueMin = valueMax;
					//console.log("valueMin = valueMax")
				}
				if (valueMin < sliderMinRange) {
					valueMin = sliderMinRange;
					//console.log("valueMin = 500;")
				}
				if (valueMax > sliderMaxRange) {
					valueMax = sliderMaxRange;
					//console.log("valueMax = 20000;")
				}
				//console.log(valueMin);
				//console.log(valueMax);
				//console.log("slider values " + $( "#slider-range" ).slider( "option", "values" ));

				// set slider values = input values
				$("#slider-range").slider("option", "values", [valueMin, valueMax]);

				// set input values = slider values

				$("#amount__min").val(valueMin);
				$("#amount__max").val(valueMax);
			});


		});
	})();
	// fillter price slider end 


	// js-product-grid-slider
	var productGridSlider = $('.js-product-grid-slider').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: true,
		dots: false,
		infinite: true,
		lazyLoad: 'ondemand',
		nextArrow: '<button type="button" class="product-grid-slider__btn--next product-grid-slider__btn">' + arrowRight + '</button>',
		prevArrow: '<button type="button" class="product-grid-slider__btn--prev product-grid-slider__btn">' + arrowLeft + '</button>',
		swipeToSlide: true,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 2
				}
    },
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1
				}
    }
  ]
	});
	// js-product-grid-slider end

	// js-img-slider
	var imgSlider = $('.js-img-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		dots: false,
		infinite: true,
		lazyLoad: 'ondemand',
		nextArrow: '<button type="button" class="slider__btn--next slider__btn">' + arrowRight + '</button>',
		prevArrow: '<button type="button" class="slider__btn--prev slider__btn">' + arrowLeft + '</button>',
		swipeToSlide: true
	});
	// js-img-slider end

	// js-brand-slider
	var brandSlider = $('.js-brand-slider');

			brandSlider.slick({
				slidesToShow: 5,
				slidesToScroll: 1,
				rows: 2,
				arrows: true,
				dots: false,
				infinite: true,
				lazyLoad: 'ondemand',
				nextArrow: '<button type="button" class="slider__btn--next slider__btn">' + arrowRight + '</button>',
				prevArrow: '<button type="button" class="slider__btn--prev slider__btn">' + arrowLeft + '</button>',
				swipeToSlide: true,
				responsive: [
					{
						breakpoint: 992,
						settings: {
							slidesToShow: 2
						}
		    },
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 1
						}
		    }
		  ]
		});


		
	// js-brand-slider end
	
	// js-prod-slider
	var $gridSliders = $(".js-grid-slider");

	if($gridSliders.length) {
		$gridSliders.each(function (i, el) {
			var $gallerySlider = $(el).find(".js-prod-gallery-slider");
			var $imagesSlider = $(el).find(".js-prod-image-slider");
			var $gallerySliderIndex = $gallerySlider.addClass("js-prod-gallery-slider--" + i);
			var $imagesSliderIndex = $imagesSlider.addClass("js-prod-image-slider--" + i);
			//console.log($gallerySliderIndex);
			//console.log($imagesSliderIndex);
			initGridSlider($gallerySliderIndex, $imagesSliderIndex);
		});// end .each()
	}

	function initGridSlider($gallerySliderIndex, $imagesSliderIndex) {
		var $gallerySlider = $gallerySliderIndex;
		$gallerySlider.slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			arrows: false,
			dots: false,
			lazyLoad: 'ondemand',
			vertical: true,
			infinite: true,
			verticalSwiping: true,
			swipeToSlide: true,
			focusOnSelect: true,
			asNavFor: $imagesSliderIndex,
			responsive: [

				{
					breakpoint: 768,
					settings: {
						vertical: false
					}
    }
  ]
		});
		var $imagesSlider = $imagesSliderIndex;
		$imagesSlider.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			dots: false,
			fade: true,
			infinite: true,
			lazyLoad: 'ondemand',
			asNavFor: $gallerySliderIndex,
			swipe: true,
			swipeToSlide: false
		});
		//console.log("initProdSlider()");
	}
	// js-prod-slider end

	// jquery ui tabs
	$(".js-ui-tabs").tabs({
		beforeActivate: function (event, ui) {
			productGridSlider.slick("slickGoTo", 0); // fix for slick slider
		}
	});
	// jquery ui tabs end

	function initForms() {
		// tsr-select
		var select = $(".tsr-select");
		select.styler();
		// tsr-select end

		// tsr-quantity-forms increment
		function initQuantityForm() {
			var input = $(".tsr-quantity-forms > input");

			if (input.hasClass("quantity-forms-init")) {
				return;
			} else {
				$(".tsr-quantity-forms__inc, .tsr-quantity-forms__dec").on("click", function () {
					var $button = $(this);
					var oldValue = $button.parent().find("input").val();
					if ($button.hasClass("tsr-quantity-forms__inc")) {
						var newVal = parseFloat(oldValue) + 1;
					} else if ($button.hasClass("tsr-quantity-forms__dec")) {
						if (oldValue > 1) {
							var newVal = parseFloat(oldValue) - 1;
						} else {
							newVal = 1;
						}
					}
					$button.parent().find("input").val(newVal).trigger("change");
				});
				input.addClass("quantity-forms-init");
			}

		}
		initQuantityForm();
		// tsr-quantity-forms end	

		// tsr-rating-input
		$('.js-rating-input').rating();
		// tsr-rating-input end
	}

	initForms();




	// file input controller
	(function () {
		//var file_api = ( window.File && window.FileReader && window.FileList && window.Blob ) ? true : false;
		var fileInput = $('#file_btn');
		var fileInputLabel = $('.file-input__label');

		var openFile = function (event) {
			var input = event.target;
			if (input.files[0].type.match('image.*')) {
				//console.log("img");
				var reader = new FileReader();
				var dataURL;
				reader.onload = function () {
					dataURL = reader.result;
					fileInputLabel.css("background-image", "url(" + dataURL + ")");
				};
				reader.readAsDataURL(input.files[0]);
			} else {
				alert("Можно загружать только фото");
			}

		};
		fileInput.on("change", function (e) {
			openFile(e);
		});
	}());
	// file input controller end

	// pop-up init
	$(".js-popup").magnificPopup({
		type: 'inline',
		preloader: false,
		focus: false
	});

	$('.js-popup-video').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,

		fixedContentPos: false
	});
	$(".js-fast-view").magnificPopup({
		type: 'ajax',
		preloader: true,
		focus: false,
		showCloseBtn: false,
		callbacks: {
			ajaxContentAdded: function () {
				initProdSlider();
				initForms();
			}
		}
	});
	
	$(".js-pop-up-ajax").magnificPopup({
		type: 'ajax',
		preloader: true,
		focus: false,
		showCloseBtn: false,
		callbacks: {
			ajaxContentAdded: function () {
				initForms();
			}
		}
	});
	// pop-up init end

	// filter show script
	var filterBtn = $(".js-filter__title");
	var filterBody = $(".js-filter__inner");
	filterBtn.on("click", function () {
		filterBody.slideToggle();
	});
	$(window).resize(function () {
		var mql_992 = window.matchMedia("(max-width: 992px)").matches;
		//console.log(mql_768);
		if (!mql_992) {
			filterBody.attr("style", "");
		}
	});
	// filter show script end



}); // ready

// registration-success
function showRegSuccess() {
	$.magnificPopup.open({
		items: {
			src: '#registration-success'
		},
		type: 'inline',
		preloader: false,
		focus: false
	});
}
// registration-success end

$(document).ready(function () {
	var arrowLeft = '<i class="icon-arrow_left"></i>';
	var arrowRight = '<i class="icon-arrow_right"></i>';

	var	$ourBrendsSliderVideo = $(".js-our-brends-slider-video"),
			ourSlideVideo = $ourBrendsSliderVideo.find(".banner"),
			ourBrendsSliderHalf = "our-brends-slider";

	function styleBanner () {
		$ourBrendsSliderVideo.addClass(ourBrendsSliderHalf);
	}

	function initSlider () {
		$ourBrendsSliderVideo.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			infinite: true,
			centerMode: true,
			centerPadding: '290px',
			//variableWidth: true,
			lazyLoad: 'ondemand',
			nextArrow: '<button type="button" class="banner-slider__btn--next banner-slider__btn"> ' + arrowRight + ' </button>',
			prevArrow: '<button type="button" class="banner-slider__btn--prev banner-slider__btn">' + arrowLeft + '</button>',
			swipeToSlide: true,
			responsive: [
		    {
		      breakpoint: 1600,
		      settings: {
		      	centerPadding: "100px"
		      }
		    },
		    {
		      breakpoint: 1024,
		      settings: {
		      	slidesToShow: 1,
						slidesToScroll: 1,
						centerPadding: "100px"
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						centerPadding: "50px"
		      }
		    }
		  ]
		});

	$(window).resize(function () {
		$ourBrendsSliderVideo.slick('slickGoTo', 0);
	});

		$ourBrendsSliderVideo.slick('slickGoTo', 0);
	}


	function initVideo () {
		$ourBrendsSliderVideo.removeClass('our-brends-slider-full').removeClass(ourBrendsSliderHalf);
		if(ourSlideVideo.length === 1) {
			$ourBrendsSliderVideo.addClass("our-brends-slider-full");
		} else if (ourSlideVideo.length === 2) {
			styleBanner ();
		} else {
			initSlider ();
		}
	}

	var $ourBrendsSlider = $(".js-our-brands-slider"),
			$ourBrendsSliderTest = $(".js-our-brands-slider-test"),
			$ourBrendsSliderSlide = $ourBrendsSlider.find(".banner");
			var slideToShow = 2;

		if($ourBrendsSliderSlide.length === 2) {
			slideToShow = 1;
			initOurBrandsSlider();
		}

		if($ourBrendsSliderSlide.length > 2) {
			initOurBrandsSlider();
		}

	function initOurBrandsSlider () {
		$ourBrendsSlider.slick({
		  infinite: true,
		  speed: 300,
		  slidesToShow: slideToShow,
		  slidesToScroll: 1,
		  centerMode: true,
		  centerPadding: '150px',
	    nextArrow: '<button type="button" class="banner-slider__btn--next banner-slider__btn"> ' + arrowRight + ' </button>',
			prevArrow: '<button type="button" class="banner-slider__btn--prev banner-slider__btn">' + arrowLeft + '</button>',
	    variableWidth: true,
	    responsive: [{
	        breakpoint: 992,
	        settings: {
	        	centerPadding: '50px',
	        }
	    },{
	        breakpoint: 768,
	        settings: {
	            slidesToShow: 1,
	            slidesToScroll: 1,
	            centerPadding: '0px',
	        }
	    }]
	});

		$(window).resize(function () {
			$ourBrendsSlider.slick('slickGoTo', 0);
		});

		$ourBrendsSlider.slick('slickGoTo', 0);
	}

		

	$ourBrendsSliderTest.slick({
	  infinite: true,
	  speed: 300,
	  slidesToShow: 2,
	  slidesToScroll: 1,
	  centerMode: true,
	  centerPadding: '150px',
    nextArrow: '<button type="button" class="banner-slider__btn--next banner-slider__btn"> ' + arrowRight + ' </button>',
		prevArrow: '<button type="button" class="banner-slider__btn--prev banner-slider__btn">' + arrowLeft + '</button>',
    //variableWidth: true,
     responsive: [{
	        breakpoint: 992,
	        settings: {
	        	centerPadding: '100px',
	        }
	    },{
	        breakpoint: 768,
	        settings: {
	            slidesToShow: 1,
	            slidesToScroll: 1,
	            centerPadding: '50px',
	        }
	    }]
	});

		$(window).resize(function () {
			$ourBrendsSliderTest.slick('slickGoTo', 0);
		});

		$ourBrendsSliderTest.slick('slickGoTo', 0);

	initVideo ();


});
//our-brends-slider 

$(function () {
	// swipe menu
	(function () {
		var $touch = $('.js-toggle-menu');
		var $touch_animate = $('.js-toggle-menu .sandwich');
		var className = "active";
		var $menu = $('.swipe-menu');

		function showMenu() {
			$("body").addClass(className);
			$touch_animate.addClass(className);
			//console.log("show");
		}

		function hideMenu() {
			$("body").removeClass(className);
			$touch_animate.removeClass(className);
			//console.log("hide");
		}

		$touch.on('click', function (e) {
			e.preventDefault();
			e.stopPropagation();
			//console.log("click");
			//console.log(!($touch_animate.hasClass(className)));
			if (!($touch_animate.hasClass(className))) {
				showMenu();
			} else {
				hideMenu();
			}
		});
		$(".wrapper").on('click', function (e) {
			//e.preventDefault();
			e.stopPropagation();
			hideMenu();
		});
		$menu.on('click', function (e) {
			e.stopPropagation();
		});
		$(window).resize(function () {
			var wid = $(window).width();
			if (wid > 767) {
				hideMenu();
			}
		});
	})();
	// swipe menu end
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9zcmMvanMvYWNjb3JkaW9uLmpzIiwiL3NyYy9qcy9nb29nbGUtbWFwLmpzIiwiL3NyYy9qcy9tYWluLmpzIiwiL3NyYy9qcy9zd2lwZS1tZW51LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDakNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3JNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2h0QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIkKGZ1bmN0aW9uICgpIHtcclxuXHR2YXIgJGFjY3JvZGlvbiA9ICQoXCIuanMtYWNjb3JkaW9uXCIpO1xyXG5cdHZhciBjbG9zZUNsYXNzID0gXCJpcy1jbG9zZVwiO1xyXG5cdHZhciBzcGVlZCA9IDIwMDtcclxuXHR2YXIgaXNDbG9zZSA9IGZhbHNlO1xyXG5cclxuXHRmdW5jdGlvbiBvcGVuQWNjb3JkaW9uKCkge1xyXG5cdFx0JCh0aGlzKS5yZW1vdmVDbGFzcyhjbG9zZUNsYXNzKTtcclxuXHRcdCQodGhpcykuZmluZChcIi5hY2NvcmRpb25fX2NvbnRlbnRcIikuc2xpZGVEb3duKHNwZWVkKTtcclxuXHR9XHJcblx0ZnVuY3Rpb24gY2xvc2VBY2NvcmRpb24oKSB7XHJcblx0XHQkKHRoaXMpLmFkZENsYXNzKGNsb3NlQ2xhc3MpO1xyXG5cdFx0JCh0aGlzKS5maW5kKFwiLmFjY29yZGlvbl9fY29udGVudFwiKS5zbGlkZVVwKHNwZWVkKTtcclxuXHR9XHJcblxyXG5cdGlmKCRhY2Nyb2Rpb24ubGVuZ3RoKSB7XHJcblx0XHQkYWNjcm9kaW9uLmVhY2goZnVuY3Rpb24gKGksIGVsKSB7XHJcblx0XHRcdHZhciBidG4gPSAkKGVsKS5maW5kKFwiLmFjY3JvZGlvbl9fYnRuXCIpO1xyXG5cclxuXHRcdFx0aWYoIWlzQ2xvc2UpIHtcclxuXHRcdFx0XHRjbG9zZUFjY29yZGlvbi5jYWxsKGVsKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0YnRuLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdGNvbnNvbGUubG9nKFwiY2xpY2tcIik7XHJcblx0XHRcdFx0aWYoJChlbCkuaGFzQ2xhc3MoY2xvc2VDbGFzcykpIHtcclxuXHRcdFx0XHRcdG9wZW5BY2NvcmRpb24uY2FsbChlbCk7XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdGNsb3NlQWNjb3JkaW9uLmNhbGwoZWwpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblx0XHR9KTsvLyBlbmQgLmVhY2goKVxyXG5cdH1cclxufSk7IiwiLy8gZ29vZ2xlIG1hcCBhcGlcclxuLy9BSXphU3lESEhVbmxzbngzY09LZFVpejNjbUg4ejBWMlZMT0Qtb0FcclxuLy9cclxudmFyIHN2Z0Nsb3NlSWNvbiA9ICc8c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPVwibmV3IDAgMCA1MTIgNTEyXCIgaWQ9XCJMYXllcl8xXCIgdmVyc2lvbj1cIjEuMVwiIHZpZXdCb3g9XCIwIDAgNTEyIDUxMlwiIHhtbDpzcGFjZT1cInByZXNlcnZlXCIgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiIHhtbG5zOnhsaW5rPVwiaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGlua1wiIHdpZHRoPVwiNTEyXCIgaGVpZ2h0PVwiNTEyXCI+IDxnPlx0PHBhdGggZD1cIk01MDEuNSw1MTJjLTIuNywwLTUuNC0xLTcuNS0zLjFMMy4xLDE4Qy0xLDEzLjktMSw3LjIsMy4xLDMuMUM3LjItMSwxMy45LTEsMTgsMy4xTDUwOC45LDQ5NCAgIGM0LjEsNC4xLDQuMSwxMC44LDAsMTQuOUM1MDYuOSw1MTEsNTA0LjIsNTEyLDUwMS41LDUxMnpcIiBmaWxsPVwiIzZBNkU3Q1wiIC8+PHBhdGggZD1cIk0xMC41LDUxMmMtMi43LDAtNS40LTEtNy41LTMuMWMtNC4xLTQuMS00LjEtMTAuOCwwLTE0LjlMNDk0LDMuMWM0LjEtNC4xLDEwLjgtNC4xLDE0LjksMCAgIGM0LjEsNC4xLDQuMSwxMC44LDAsMTQuOUwxOCw1MDguOUMxNS45LDUxMSwxMy4yLDUxMiwxMC41LDUxMnpcIiBmaWxsPVwiIzZBNkU3Q1wiIC8+PC9nPjwvc3ZnPic7XHJcbnZhciBtYXJrZXJzSW5zdGFuY2UgPSB7fTtcclxuXHJcbmZ1bmN0aW9uIGNsb3NlSW5mb1dpbmRvdygpIHtcclxuXHRpZiAoaW5mb1dpbmRvdykge1xyXG5cdFx0aW5mb1dpbmRvdy5jbG9zZSgpO1xyXG5cdFx0Zm9yICh2YXIgbWFya2VyS2V5IGluIG1hcmtlcnNJbnN0YW5jZSkge1xyXG5cdFx0XHRtYXJrZXJzSW5zdGFuY2VbbWFya2VyS2V5XS5zZXRJY29uKFwiaW1nL2ljb24vbWFya2VyLnBuZ1wiKTtcclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcbmZ1bmN0aW9uICBjdXN0b21pemVJbmZvV2luZG93KGluZm9XaW5kb3cpIHtcclxuXHQvLyAqXHJcblx0Ly8gU1RBUlQgSU5GT1dJTkRPVyBDVVNUT01JWkUuXHJcblx0Ly8gVGhlIGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKCkgZXZlbnQgZXhwZWN0c1xyXG5cdC8vIHRoZSBjcmVhdGlvbiBvZiB0aGUgaW5mb3dpbmRvdyBIVE1MIHN0cnVjdHVyZSAnZG9tcmVhZHknXHJcblx0Ly8gYW5kIGJlZm9yZSB0aGUgb3BlbmluZyBvZiB0aGUgaW5mb3dpbmRvdywgZGVmaW5lZCBzdHlsZXMgYXJlIGFwcGxpZWQuXHJcblx0Ly8gKlxyXG5cdGlmIChpbmZvV2luZG93KSB7XHJcblxyXG5cdFx0Z29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIoaW5mb1dpbmRvdywgJ2RvbXJlYWR5JywgZnVuY3Rpb24gKCkge1xyXG5cclxuXHRcdFx0Ly8gUmVmZXJlbmNlIHRvIHRoZSBESVYgdGhhdCB3cmFwcyB0aGUgYm90dG9tIG9mIGluZm93aW5kb3dcclxuXHRcdFx0dmFyIGl3T3V0ZXIgPSAkKCcuZ20tc3R5bGUtaXcnKTtcclxuXHJcblx0XHRcdC8qIFNpbmNlIHRoaXMgZGl2IGlzIGluIGEgcG9zaXRpb24gcHJpb3IgdG8gLmdtLWRpdiBzdHlsZS1pdy5cclxuXHRcdFx0ICogV2UgdXNlIGpRdWVyeSBhbmQgY3JlYXRlIGEgaXdCYWNrZ3JvdW5kIHZhcmlhYmxlLFxyXG5cdFx0XHQgKiBhbmQgdG9vayBhZHZhbnRhZ2Ugb2YgdGhlIGV4aXN0aW5nIHJlZmVyZW5jZSAuZ20tc3R5bGUtaXcgZm9yIHRoZSBwcmV2aW91cyBkaXYgd2l0aCAucHJldigpLlxyXG5cdFx0XHQgKi9cclxuXHRcdFx0dmFyIGl3QmFja2dyb3VuZCA9IGl3T3V0ZXIucHJldigpO1xyXG5cclxuXHRcdFx0Ly8gUmVtb3ZlcyBiYWNrZ3JvdW5kIHNoYWRvdyBESVZcclxuXHRcdFx0aXdCYWNrZ3JvdW5kLmNoaWxkcmVuKCc6bnRoLWNoaWxkKDIpJykuY3NzKHsnZGlzcGxheSc6ICdub25lJ30pO1xyXG5cclxuXHRcdFx0Ly8gUmVtb3ZlcyB3aGl0ZSBiYWNrZ3JvdW5kIERJVlxyXG5cdFx0XHRpd0JhY2tncm91bmQuY2hpbGRyZW4oJzpudGgtY2hpbGQoNCknKS5jc3MoeydkaXNwbGF5JzogJ25vbmUnfSk7XHJcblxyXG5cdFx0XHQvLyBNb3ZlcyB0aGUgaW5mb3dpbmRvdyAxMTVweCB0byB0aGUgcmlnaHQuXHJcblx0XHRcdC8vaXdPdXRlci5wYXJlbnQoKS5wYXJlbnQoKS5jc3Moe2xlZnQ6ICcwcHgnfSk7XHJcblxyXG5cdFx0XHQvLyBNb3ZlcyB0aGUgc2hhZG93IG9mIHRoZSBib3R0b20gYXJyb3cuXHJcblx0XHRcdGl3QmFja2dyb3VuZC5jaGlsZHJlbignOm50aC1jaGlsZCgxKScpLmNzcygnZGlzcGxheScsIFwibm9uZVwiKTtcclxuXHRcdFx0Ly8gUmVtb3ZlIHRoZSBib3R0b20gYXJyb3cuXHJcblxyXG5cdFx0XHRpd0JhY2tncm91bmQuY2hpbGRyZW4oJzpudGgtY2hpbGQoMyknKS5jc3MoJ2Rpc3BsYXknLCBcIm5vbmVcIik7XHJcblxyXG5cdFx0XHQvLyBDaGFuZ2VzIHRoZSBkZXNpcmVkIHRhaWwgc2hhZG93IGNvbG9yLlxyXG5cdFx0XHQvKml3QmFja2dyb3VuZC5jaGlsZHJlbignOm50aC1jaGlsZCgzKScpLmZpbmQoJ2RpdicpLmNoaWxkcmVuKCkuY3NzKHtcclxuXHRcdFx0XHQnYm94LXNoYWRvdyc6ICdyZ2JhKDcyLCAxODEsIDIzMywgMC42KSAwcHggMXB4IDZweCcsXHJcblx0XHRcdFx0J3otaW5kZXgnOiAnMSdcclxuXHRcdFx0fSk7Ki9cclxuXHJcblx0XHRcdC8vIFJlZmVyZW5jZSB0byB0aGUgZGl2IHRoYXQgZ3JvdXBzIHRoZSBjbG9zZSBidXR0b24gZWxlbWVudHMuXHJcblx0XHRcdHZhciBpd0Nsb3NlQnRuID0gaXdPdXRlci5uZXh0KCk7XHJcblx0XHRcdC8vIGhpZGUgY2xvc2UgYnRuXHJcblx0XHRcdGl3Q2xvc2VCdG4uY3NzKCdkaXNwbGF5JywgXCJub25lXCIpO1xyXG5cdFx0XHQvLyBBcHBseSB0aGUgZGVzaXJlZCBlZmZlY3QgdG8gdGhlIGNsb3NlIGJ1dHRvblxyXG5cdFx0XHQvLyBpd0Nsb3NlQnRuLmNzcyh7XHJcblx0XHRcdC8vIFx0b3BhY2l0eTogJzEnLFxyXG5cdFx0XHQvLyBcdHJpZ2h0OiAnMTBweCcsXHJcblx0XHRcdC8vIFx0dG9wOiAnMTBweCcsXHJcblx0XHRcdC8vIFx0Ym9yZGVyOiAnbm9uZSdcclxuXHRcdFx0Ly8gfSk7XHJcblxyXG5cdFx0XHQvLyBJZiB0aGUgY29udGVudCBvZiBpbmZvd2luZG93IG5vdCBleGNlZWQgdGhlIHNldCBtYXhpbXVtIGhlaWdodCwgdGhlbiB0aGUgZ3JhZGllbnQgaXMgcmVtb3ZlZC5cclxuXHRcdFx0LyppZiAoJCgnLml3LWNvbnRlbnQnKS5oZWlnaHQoKSA8IDE0MCkge1xyXG5cdFx0XHRcdCQoJy5pdy1ib3R0b20tZ3JhZGllbnQnKS5jc3Moe2Rpc3BsYXk6ICdub25lJ30pO1xyXG5cdFx0XHR9Ki9cclxuXHJcblx0XHRcdC8vIFRoZSBBUEkgYXV0b21hdGljYWxseSBhcHBsaWVzIDAuNyBvcGFjaXR5IHRvIHRoZSBidXR0b24gYWZ0ZXIgdGhlIG1vdXNlb3V0IGV2ZW50LiBUaGlzIGZ1bmN0aW9uIHJldmVyc2VzIHRoaXMgZXZlbnQgdG8gdGhlIGRlc2lyZWQgdmFsdWUuXHJcblx0XHRcdGl3Q2xvc2VCdG4ubW91c2VvdXQoZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdCQodGhpcykuY3NzKHtvcGFjaXR5OiAnMSd9KTtcclxuXHRcdFx0fSk7XHJcblx0XHR9KTtcclxuXHR9XHJcbn1cclxuXHJcbnZhciBpbmZvV2luZG93ID0gbnVsbDtcclxuZnVuY3Rpb24gYWRkSW5mb1dpbmRvdyhtYXJrZXIsIG1lc3NhZ2UpIHtcclxuXHJcblx0Ly8gc2hvdyBpbmZvV2luZG93XHJcblx0Z29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIobWFya2VyLCAnY2xpY2snLCBmdW5jdGlvbiAoKSB7XHJcblx0XHRjbG9zZUluZm9XaW5kb3coKTtcclxuXHJcblx0XHRpbmZvV2luZG93ID0gbmV3IGdvb2dsZS5tYXBzLkluZm9XaW5kb3coe1xyXG5cdFx0XHRjb250ZW50OiAnPGRpdiBjbGFzcz1cImJhbGxvblwiPjxzcGFuIGNsYXNzPVwiYmFsbG9uX19jbG9zZVwiIG9uY2xpY2s9XCJjbG9zZUluZm9XaW5kb3coKTtcIj4nICsgc3ZnQ2xvc2VJY29uICsgJzwvc3Bhbj4nICsgbWVzc2FnZSArICc8L2Rpdj4nLFxyXG5cdFx0XHRtYXhXaWR0aDogNDAwXHJcblx0XHR9KTtcclxuXHRcdC8vIHNldCBjdXN0b20gc3R5bGVzXHJcblx0XHRjdXN0b21pemVJbmZvV2luZG93KGluZm9XaW5kb3cpO1xyXG5cdFx0Ly8gaGlkZSBtYXJrZXJcclxuXHRcdG1hcmtlci5zZXRJY29uKFwiaW1nL2ljb24vbWFya2VyLWVtcHR5LnBuZ1wiKTtcclxuXHJcblx0XHQvLyB3YWl0aW5nIHN0eWxlc1xyXG5cdFx0c2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcblx0XHRcdGluZm9XaW5kb3cub3BlbihnTWFwLCBtYXJrZXIpO1xyXG5cdFx0fSwgNzApO1xyXG5cclxuXHR9KTtcclxuXHJcbn0vL2FkZEluZm9XaW5kb3dcclxuXHJcbmZ1bmN0aW9uIGluaXRNYXAobWFwQ29uZmlnKSB7XHJcblxyXG5cdC8vIFNwZWNpZnkgZmVhdHVyZXMgYW5kIGVsZW1lbnRzIHRvIGRlZmluZSBzdHlsZXMuXHJcblx0dmFyIHN0eWxlQXJyYXkgPSBbXHJcblx0XHR7XHJcblx0XHRcdGZlYXR1cmVUeXBlOiBcImFsbFwiLFxyXG5cdFx0XHRzdHlsZXJzOiBbXHJcblx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0c2F0dXJhdGlvbjogMFxyXG5cdFx0XHRcdH1cclxuXHRcdFx0XVxyXG5cdFx0fSwge1xyXG5cdFx0XHRmZWF0dXJlVHlwZTogXCJwb2kuYnVzaW5lc3NcIixcclxuXHRcdFx0Ly9lbGVtZW50VHlwZTogXCJsYWJlbHNcIixcclxuXHRcdFx0c3R5bGVyczogW1xyXG5cdFx0XHRcdHtcclxuXHRcdFx0XHRcdHZpc2liaWxpdHk6IFwib2ZmXCJcclxuXHRcdFx0XHR9XHJcblx0XHRcdF1cclxuXHRcdH1cclxuXHRdO1xyXG5cclxuXHR2YXIgaXNEcmFnZ2FibGUgPSAkKGRvY3VtZW50KS53aWR0aCgpID4gNzY4ID8gdHJ1ZSA6IGZhbHNlOyAvLyBJZiBkb2N1bWVudCAoeW91ciB3ZWJzaXRlKSBpcyB3aWRlciB0aGFuIDc2OHB4LCBpc0RyYWdnYWJsZSA9IHRydWUsIGVsc2UgaXNEcmFnZ2FibGUgPSBmYWxzZVxyXG5cclxuXHR2YXIgbWFwT3B0aW9ucyA9IHtcclxuXHRcdGRyYWdnYWJsZTogaXNEcmFnZ2FibGUsXHJcblx0XHRjZW50ZXI6IHtcclxuXHRcdFx0bGF0OiBtYXBDb25maWcubWFwQ2VudGVyWzBdLFxyXG5cdFx0XHRsbmc6IG1hcENvbmZpZy5tYXBDZW50ZXJbMV1cclxuXHRcdH0sXHJcblx0XHQvLyBBcHBseSB0aGUgbWFwIHN0eWxlIGFycmF5IHRvIHRoZSBtYXAuXHJcblx0XHRzdHlsZXM6IHN0eWxlQXJyYXksXHJcblx0XHR6b29tOiBtYXBDb25maWcuem9vbSxcclxuXHRcdG1hcFR5cGVJZDogZ29vZ2xlLm1hcHMuTWFwVHlwZUlkLlJPQURNQVAsXHJcblx0XHRwYW5Db250cm9sOiBmYWxzZSxcclxuXHRcdHpvb21Db250cm9sOiB0cnVlLFxyXG5cdFx0bWFwVHlwZUNvbnRyb2w6IGZhbHNlLFxyXG5cdFx0c2NhbGVDb250cm9sOiBmYWxzZSxcclxuXHRcdHN0cmVldFZpZXdDb250cm9sOiB0cnVlLFxyXG5cdFx0b3ZlcnZpZXdNYXBDb250cm9sOiBmYWxzZSxcclxuXHRcdHNjcm9sbHdoZWVsOiBmYWxzZSAvLyBQcmV2ZW50IHVzZXJzIHRvIHN0YXJ0IHpvb21pbmcgdGhlIG1hcCB3aGVuIHNjcm9sbGluZyBkb3duIHRoZSBwYWdlXHJcblx0XHQvLy4uLiBvcHRpb25zIG9wdGlvbnMgb3B0aW9uc1xyXG5cdH07XHJcblx0Ly8gQ3JlYXRlIGEgbWFwIG9iamVjdCBhbmQgc3BlY2lmeSB0aGUgRE9NIGVsZW1lbnQgZm9yIGRpc3BsYXkuXHJcblx0Z01hcCA9IG5ldyBnb29nbGUubWFwcy5NYXAoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2ctbWFwJyksIG1hcE9wdGlvbnMpO1xyXG5cclxuXHQvLyBFdmVudCB0aGF0IGNsb3NlcyB0aGUgSW5mbyBXaW5kb3cgd2l0aCBhIGNsaWNrIG9uIHRoZSBtYXBcclxuXHRnb29nbGUubWFwcy5ldmVudC5hZGRMaXN0ZW5lcihnTWFwLCAnY2xpY2snLCBmdW5jdGlvbiAoKSB7XHJcblx0XHRjbG9zZUluZm9XaW5kb3coKTtcclxuXHR9KTtcclxuXHJcblx0Ly8gY3JlYXRlIG1hcmtlcnNcclxuXHR2YXIgbWFya2VycyA9IG1hcENvbmZpZy5tYXJrZXJzO1xyXG5cdHZhciBtYXJrZXJQb3NpdGlvbiA9IHt9O1xyXG5cclxuXHRmb3IgKHZhciBtYXJrZXIgaW4gbWFya2Vycykge1xyXG5cclxuXHRcdC8vIC8vIGNyZWF0ZSBuZXcgbWFya2VyIHBvc2l0aW9uIGluc3RhbmNlXHJcblx0XHRtYXJrZXJQb3NpdGlvblttYXJrZXJdID0gbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhtYXJrZXJzW21hcmtlcl0uY2VudGVyWzBdLCBtYXJrZXJzW21hcmtlcl0uY2VudGVyWzFdKTtcclxuXHRcdC8vIGNyZWF0ZSBuZXcgbWFya2VyIGluc3RhbmNlXHJcblx0XHRtYXJrZXJzSW5zdGFuY2VbbWFya2VyXSA9IG5ldyBnb29nbGUubWFwcy5NYXJrZXIoe1xyXG5cdFx0XHRwb3NpdGlvbjogbWFya2VyUG9zaXRpb25bbWFya2VyXSxcclxuXHRcdFx0bWFwOiBnTWFwLFxyXG5cdFx0XHR0aXRsZTogbWFya2Vyc1ttYXJrZXJdLnRpdGxlLFxyXG5cdFx0XHRpY29uOiBcImltZy9pY29uL21hcmtlci5wbmdcIlxyXG5cdFx0fSk7XHJcblxyXG5cdFx0Ly8gY3JlYXRlIGFuZCBzaG93IGluZm9XaW5kb3cgb24gY2xpY2tcclxuXHRcdGFkZEluZm9XaW5kb3cobWFya2Vyc0luc3RhbmNlW21hcmtlcl0sIG1hcmtlcnNbbWFya2VyXS5iYWxvb25Db250ZW50KTtcclxuXHJcblxyXG5cdFx0Ly9jb25zb2xlLmxvZyhtYXJrZXJzSW5zdGFuY2VbbWFya2VyXSk7XHJcblx0fVxyXG5cclxuXHJcbn0vLyBpbml0TWFwXHJcblxyXG5mdW5jdGlvbiBjZW50ZXJpbmdNYXJrZXIobGF0aXR1ZGUsIGxvbmdpdHVkZSkge1xyXG5cdC8vY2VudGVyaW5nIG1hcFxyXG5cdC8vIGJlY2F1c2UgZ2xhZGlvbHVzXHJcblx0Ly9nTWFwLnNldENlbnRlcih7bGF0OiBsYXRpdHVkZSwgbG5nOiBsb25naXR1ZGV9KTtcclxuXHRnTWFwLnBhblRvKHtsYXQ6IGxhdGl0dWRlLCBsbmc6IGxvbmdpdHVkZX0pO1xyXG5cdC8vcmV0dXJuIGZhbHNlO1xyXG59XHJcblxyXG4kKHdpbmRvdykub24oXCJsb2FkXCIsIGZ1bmN0aW9uICgpIHtcclxuXHRpZiAodHlwZW9mIG1hcENvbmZpZyAhPSBcInVuZGVmaW5lZFwiKSB7XHJcblx0XHR2YXIgZ01hcDtcclxuXHRcdGluaXRNYXAobWFwQ29uZmlnKTtcclxuXHR9XHJcbn0pO1xyXG4vLyBnb29nbGUgbWFwIGFwaSBlbmQiLCIkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XHJcblx0dmFyIGFycm93TGVmdCA9ICc8aSBjbGFzcz1cImljb24tYXJyb3dfbGVmdFwiPjwvaT4nO1xyXG5cdHZhciBhcnJvd1JpZ2h0ID0gJzxpIGNsYXNzPVwiaWNvbi1hcnJvd19yaWdodFwiPjwvaT4nO1xyXG5cclxuXHR2YXIgYmFubmVyU2xpZGVyID0gJCgnLmpzLWJhbm5lci1zbGlkZXInKS5zbGljayh7XHJcblx0XHRzbGlkZXNUb1Nob3c6IDEsXHJcblx0XHRzbGlkZXNUb1Njcm9sbDogMSxcclxuXHRcdGFycm93czogdHJ1ZSxcclxuXHRcdGluZmluaXRlOiB0cnVlLFxyXG5cdFx0Y2VudGVyTW9kZTogdHJ1ZSxcclxuXHRcdGNlbnRlclBhZGRpbmc6ICcwJyxcclxuXHRcdC8vdmFyaWFibGVXaWR0aDogdHJ1ZSxcclxuXHRcdGxhenlMb2FkOiAnb25kZW1hbmQnLFxyXG5cdFx0bmV4dEFycm93OiAnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJiYW5uZXItc2xpZGVyX19idG4tLW5leHQgYmFubmVyLXNsaWRlcl9fYnRuXCI+ICcgKyBhcnJvd1JpZ2h0ICsgJyA8L2J1dHRvbj4nLFxyXG5cdFx0cHJldkFycm93OiAnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJiYW5uZXItc2xpZGVyX19idG4tLXByZXYgYmFubmVyLXNsaWRlcl9fYnRuXCI+JyArIGFycm93TGVmdCArICc8L2J1dHRvbj4nLFxyXG5cdFx0c3dpcGVUb1NsaWRlOiB0cnVlXHJcblx0fSk7XHJcblx0JCh3aW5kb3cpLnJlc2l6ZShmdW5jdGlvbiAoKSB7XHJcblx0XHRiYW5uZXJTbGlkZXIuc2xpY2soJ3NsaWNrR29UbycsIDApO1xyXG5cdH0pO1xyXG5cdGJhbm5lclNsaWRlci5zbGljaygnc2xpY2tHb1RvJywgMCk7XHJcblxyXG5cdC8vIGNhcnQgbW9kdWxlIHN0YXJ0XHJcblx0dmFyICRjYXJ0ID0gJChcIi5qcy1jYXJ0XCIpO1xyXG5cdHZhciAkY2FydExhYmVsID0gJChcIi5qcy1jYXJ0LW9wZW4tY2xvc2UtbGFiZWxcIik7XHJcblxyXG5cdGZ1bmN0aW9uIG9wZW5DYXJ0KCkge1xyXG5cdFx0JGNhcnQucmVtb3ZlQ2xhc3MoXCJjYXJ0LS1jbG9zZVwiKTtcclxuXHRcdCRjYXJ0LmFkZENsYXNzKFwiY2FydC0tb3BlblwiKTtcclxuXHR9XHJcblxyXG5cdGZ1bmN0aW9uIGNsb3NlQ2FydCgpIHtcclxuXHRcdCRjYXJ0LnJlbW92ZUNsYXNzKFwiY2FydC0tb3BlblwiKTtcclxuXHRcdCRjYXJ0LmFkZENsYXNzKFwiY2FydC0tY2xvc2VcIik7XHJcblx0fVxyXG5cclxuXHRmdW5jdGlvbiBjaGVja0NhcnRQb3NpdGlvbigpIHtcclxuXHRcdHZhciBib3R0b21FZGdlID0gJChcIi5mb290ZXJcIikub2Zmc2V0KCkudG9wO1xyXG5cdFx0dmFyIGNhcnRQb3NpdGlvbkJvdHRvbSA9ICQod2luZG93KS5zY3JvbGxUb3AoKSArICQod2luZG93KS5oZWlnaHQoKTtcclxuXHRcdHZhciBzY3JvbGxQb3NpdGlvbkJvdG9tO1xyXG5cdFx0aWYgKGJvdHRvbUVkZ2UgPCBjYXJ0UG9zaXRpb25Cb3R0b20pIHtcclxuXHRcdFx0JGNhcnQuY3NzKHtcclxuXHRcdFx0XHRcImJvdHRvbVwiOiBjYXJ0UG9zaXRpb25Cb3R0b20gLSBib3R0b21FZGdlLFxyXG5cdFx0XHRcdFwib3BhY2l0eVwiOiAwLFxyXG5cdFx0XHRcdFwicG9pbnRlci1ldmVudHNcIjogXCJub25lXCJcclxuXHRcdFx0fSk7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHQkY2FydC5jc3Moe1xyXG5cdFx0XHRcdFwiYm90dG9tXCI6IDAsXHJcblx0XHRcdFx0XCJvcGFjaXR5XCI6IDEsXHJcblx0XHRcdFx0XCJwb2ludGVyLWV2ZW50c1wiOiBcImF1dG9cIlxyXG5cdFx0XHR9KTtcclxuXHRcdH1cclxuXHR9XHJcblx0Y2hlY2tDYXJ0UG9zaXRpb24oKTtcclxuXHQvLyBjYXJ0IGxpc3RlbmVycyBcclxuXHQkY2FydExhYmVsLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xyXG5cdFx0aWYgKCRjYXJ0Lmhhc0NsYXNzKFwiY2FydC0tY2xvc2VcIikpIHtcclxuXHRcdFx0b3BlbkNhcnQoKTtcclxuXHRcdH0gZWxzZSBpZiAoJGNhcnQuaGFzQ2xhc3MoXCJjYXJ0LS1vcGVuXCIpKSB7XHJcblx0XHRcdGNsb3NlQ2FydCgpO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0Y29uc29sZS5lcnJvcihcImNhcnQgZXJyb3IgaW4gY2xhc3MhXCIpO1xyXG5cdFx0fVxyXG5cdH0pO1xyXG5cdCRjYXJ0Lm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24gKGUpIHtcclxuXHRcdGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcblx0fSk7XHJcblx0JChcIi53cmFwcGVyXCIpLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24gKGUpIHtcclxuXHRcdGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcblx0XHRjbG9zZUNhcnQoKTtcclxuXHR9KTtcclxuXHJcblx0LypcclxuXHQkKHdpbmRvdykub24oXCJzY3JvbGxcIiwgZnVuY3Rpb24oZSkge1xyXG5cdFx0Y2xvc2VDYXJ0KCk7XHJcblx0fSk7ICovXHJcblx0JCh3aW5kb3cpLm9uKFwicmVzaXplXCIsIGZ1bmN0aW9uIChlKSB7XHJcblx0XHRjbG9zZUNhcnQoKTtcclxuXHRcdGNoZWNrQ2FydFBvc2l0aW9uKCk7XHJcblx0fSk7XHJcblxyXG5cdCQod2luZG93KS5vbihcInNjcm9sbFwiLCBmdW5jdGlvbiAoKSB7XHJcblx0XHRjaGVja0NhcnRQb3NpdGlvbigpO1xyXG5cdH0pO1xyXG5cdC8vIGNhcnQgbGlzdGVuZXJzIGVuZFxyXG5cdC8vIGN1c3RvbSBzY3JvbGxiYXIgaW4gY2FydFxyXG5cdCQod2luZG93KS5sb2FkKGZ1bmN0aW9uICgpIHtcclxuXHRcdCQoXCIuanMtY3VzdG9tLXNjcm9sbGJhclwiKS5tQ3VzdG9tU2Nyb2xsYmFyKHtcclxuXHRcdFx0c2V0SGVpZ2h0OiBcIjEwMCVcIlxyXG5cdFx0fSk7XHJcblx0fSk7XHJcblxyXG5cdC8vY2FydCBtb2R1bGUgZW5kXHJcblxyXG5cdC8qIHNtb290aCBzY3JvbGxpbmcgKi9cclxuXHQoZnVuY3Rpb24gKCkge1xyXG5cdFx0dmFyIHRhcmdldCA9ICQoJ2FbaHJlZl49XCIjXCJdJyk7XHJcblxyXG5cdFx0ZnVuY3Rpb24gc21vb3RoU2Nyb2xsKCkge1xyXG5cdFx0XHQvLyBhZGQgXCJzbW9vdGhfc2Nyb2xsXCIgY2xhc3MgdG8gdGhlIGxpbmsgd2l0aCBocmVmPVwiI2Rlc3RpbmF0aW9uXCJcclxuXHRcdFx0ZWxlbWVudENsaWNrID0gJCh0aGlzKS5hdHRyKFwiaHJlZlwiKTtcclxuXHRcdFx0ZGVzdGluYXRpb24gPSAkKGVsZW1lbnRDbGljaykub2Zmc2V0KCkudG9wO1xyXG5cdFx0XHQkKCdib2R5JykuYW5pbWF0ZSh7XHJcblx0XHRcdFx0c2Nyb2xsVG9wOiBkZXN0aW5hdGlvblxyXG5cdFx0XHR9LCAxMTAwKTtcclxuXHRcdFx0JCgnaHRtbCcpLmFuaW1hdGUoe1xyXG5cdFx0XHRcdHNjcm9sbFRvcDogZGVzdGluYXRpb25cclxuXHRcdFx0fSwgMTEwMCk7XHJcblx0XHRcdHJldHVybiBmYWxzZTtcclxuXHRcdH1cclxuXHRcdHRhcmdldC5vbihcImNsaWNrXCIsIHNtb290aFNjcm9sbCk7XHJcblx0fSkoKTtcclxuXHQvKiBzbW9vdGggc2Nyb2xsaW5nIGVuZCovXHJcblxyXG5cdC8vIGNhdGVnb3J5IG1lbnUgXHJcblx0dmFyIG1haW5NZW51QnRuID0gJChcIi5jYXRlZ29yeS1tZW51X19tb2JpbGUtdGl0bGVcIik7XHJcblx0dmFyIHN1Yk1lbnVCdG4gPSAkKFwiLmNhdGVnb3J5LW1lbnVfX2l0ZW0gPiBpXCIpO1xyXG5cdHZhciBtZW51Qm9keSA9ICQoXCIuY2F0ZWdvcnktbWVudV9fbGlzdFwiKTtcclxuXHJcblx0ZnVuY3Rpb24gc2hvd0NhdE1lbnUoKSB7XHJcblx0XHRtZW51Qm9keS5zbGlkZURvd24oMjAwKTtcclxuXHRcdG1haW5NZW51QnRuLmZpbmQoXCIgPiBpIFwiKS5yZW1vdmVDbGFzcyhcImZhLXBsdXNcIikuYWRkQ2xhc3MoXCJmYS1taW51c1wiKTtcclxuXHR9XHJcblxyXG5cdGZ1bmN0aW9uIGhpZGVDYXRNZW51KCkge1xyXG5cdFx0bWVudUJvZHkuc2xpZGVVcCgyMDApO1xyXG5cdFx0bWFpbk1lbnVCdG4uZmluZChcIiA+IGkgXCIpLnJlbW92ZUNsYXNzKFwiZmEtbWludXNcIikuYWRkQ2xhc3MoXCJmYS1wbHVzXCIpO1xyXG5cdH1cclxuXHJcblx0ZnVuY3Rpb24gc2hvd1N1Ym1lbnUobWVudUJ0bikge1xyXG5cdFx0bWVudUJ0bi5wYXJlbnQoKS5maW5kKFwiID4gdWwgXCIpLnNsaWRlRG93bigyMDApO1xyXG5cdFx0bWVudUJ0bi5yZW1vdmVDbGFzcyhcImZhLXBsdXNcIikuYWRkQ2xhc3MoXCJmYS1taW51c1wiKTtcclxuXHR9XHJcblxyXG5cdGZ1bmN0aW9uIGhpZGVTdWJtZW51KG1lbnVCdG4pIHtcclxuXHRcdG1lbnVCdG4ucGFyZW50KCkuZmluZChcIiA+IHVsIFwiKS5zbGlkZVVwKDIwMCk7XHJcblx0XHRtZW51QnRuLnJlbW92ZUNsYXNzKFwiZmEtbWludXNcIikuYWRkQ2xhc3MoXCJmYS1wbHVzXCIpO1xyXG5cdH1cclxuXHJcblx0JCh3aW5kb3cpLnJlc2l6ZShmdW5jdGlvbiAoKSB7XHJcblx0XHR2YXIgbXFsXzc2OCA9IHdpbmRvdy5tYXRjaE1lZGlhKFwiKG1heC13aWR0aDogNzY3cHgpXCIpLm1hdGNoZXM7XHJcblx0XHQvL2NvbnNvbGUubG9nKG1xbF83NjgpO1xyXG5cdFx0aWYgKCFtcWxfNzY4KSB7XHJcblx0XHRcdHNob3dDYXRNZW51KCk7XHJcblx0XHRcdHJldHVybjtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdGhpZGVDYXRNZW51KCk7XHJcblx0XHRcdHJldHVybjtcclxuXHRcdH1cclxuXHR9KTtcclxuXHRtYWluTWVudUJ0bi5vbihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcclxuXHRcdGlmICgkKHRoaXMpLmZpbmQoXCIgPiBpIFwiKS5oYXNDbGFzcyhcImZhLXBsdXNcIikpIHtcclxuXHRcdFx0c2hvd0NhdE1lbnUoKTtcclxuXHRcdFx0cmV0dXJuO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0aGlkZUNhdE1lbnUoKTtcclxuXHRcdFx0cmV0dXJuO1xyXG5cdFx0fVxyXG5cdH0pO1xyXG5cdHN1Yk1lbnVCdG4ub24oXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XHJcblx0XHRpZiAoJCh0aGlzKS5oYXNDbGFzcyhcImZhLW1pbnVzXCIpKSB7XHJcblx0XHRcdGhpZGVTdWJtZW51KCQodGhpcykpO1xyXG5cdFx0XHRyZXR1cm47XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHRoaWRlU3VibWVudShzdWJNZW51QnRuKTtcclxuXHRcdFx0c2hvd1N1Ym1lbnUoJCh0aGlzKSk7XHJcblx0XHR9XHJcblx0fSk7XHJcblx0Ly8gY2F0ZWdvcnkgbWVudSBlbmRcclxuXHJcblxyXG5cdC8vIGZvb3RlciBtZW51XHJcblx0JChcIi5mb290ZXJfX2Rvd24tYXJyb3dcIikub24oXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XHJcblx0XHQkKFwiLmZvb3Rlci1tZW51XCIpLnRvZ2dsZUNsYXNzKFwiZm9vdGVyLW1lbnUtLW9wZW5cIik7XHJcblx0fSk7XHJcblx0Ly8gZm9vdGVyIG1lbnUgZW5kXHJcblxyXG5cdC8vIGNhdGVnb3J5LXNsaWRlclxyXG5cdHZhciBjYXRlZ29yeVNsaWRlciA9ICQoJy5qcy1jYXRlZ29yeS1zbGlkZXInKS5zbGljayh7XHJcblx0XHRzbGlkZXNUb1Nob3c6IDYsXHJcblx0XHRzbGlkZXNUb1Njcm9sbDogMSxcclxuXHRcdGFycm93czogdHJ1ZSxcclxuXHRcdGRvdHM6IHRydWUsXHJcblx0XHRpbmZpbml0ZTogdHJ1ZSxcclxuXHRcdGxhenlMb2FkOiAnb25kZW1hbmQnLFxyXG5cdFx0bmV4dEFycm93OiAnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJjYXRlZ29yeS1zbGlkZXJfX2J0bi0tbmV4dCBjYXRlZ29yeS1zbGlkZXJfX2J0blwiPicgKyBhcnJvd1JpZ2h0ICsgJzwvYnV0dG9uPicsXHJcblx0XHRwcmV2QXJyb3c6ICc8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImNhdGVnb3J5LXNsaWRlcl9fYnRuLS1wcmV2IGNhdGVnb3J5LXNsaWRlcl9fYnRuXCI+JyArIGFycm93TGVmdCArICc8L2J1dHRvbj4nLFxyXG5cdFx0c3dpcGVUb1NsaWRlOiB0cnVlLFxyXG5cdFx0cmVzcG9uc2l2ZTogW1xyXG5cdFx0XHR7XHJcblx0XHRcdFx0YnJlYWtwb2ludDogNzY4LFxyXG5cdFx0XHRcdHNldHRpbmdzOiB7XHJcblx0XHRcdFx0XHRzbGlkZXNUb1Nob3c6IDNcclxuXHRcdFx0XHR9XHJcbiAgICB9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0YnJlYWtwb2ludDogNDgwLFxyXG5cdFx0XHRcdHNldHRpbmdzOiB7XHJcblx0XHRcdFx0XHRzbGlkZXNUb1Nob3c6IDFcclxuXHRcdFx0XHR9XHJcbiAgICB9XHJcbiAgXVxyXG5cdH0pO1xyXG5cdC8vIGNhdGVnb3J5LXNsaWRlciBlbmRcclxuXHJcblx0Ly8gb25seSBtdW1iZXJzIGlucHV0XHJcblx0JChcIiNhbW91bnRfX21pbiwgI2Ftb3VudF9fbWF4LCAuanMtb25seS1udW1iZXJzXCIpLmtleWRvd24oZnVuY3Rpb24gKGUpIHtcclxuXHRcdC8vIEFsbG93OiBiYWNrc3BhY2UsIGRlbGV0ZSwgdGFiLCBlc2NhcGUsIGVudGVyIGFuZCAuXHJcblx0XHRpZiAoJC5pbkFycmF5KGUua2V5Q29kZSwgWzQ2LCA4LCA5LCAyNywgMTMsIDExMCwgMTkwXSkgIT09IC0xIHx8XHJcblx0XHRcdC8vIEFsbG93OiBDdHJsK0EsIENvbW1hbmQrQVxyXG5cdFx0XHQoZS5rZXlDb2RlID09IDY1ICYmIChlLmN0cmxLZXkgPT09IHRydWUgfHwgZS5tZXRhS2V5ID09PSB0cnVlKSkgfHxcclxuXHRcdFx0Ly8gQWxsb3c6IGhvbWUsIGVuZCwgbGVmdCwgcmlnaHQsIGRvd24sIHVwXHJcblx0XHRcdChlLmtleUNvZGUgPj0gMzUgJiYgZS5rZXlDb2RlIDw9IDQwKSkge1xyXG5cdFx0XHQvLyBsZXQgaXQgaGFwcGVuLCBkb24ndCBkbyBhbnl0aGluZ1xyXG5cdFx0XHRyZXR1cm47XHJcblx0XHR9XHJcblx0XHQvLyBFbnN1cmUgdGhhdCBpdCBpcyBhIG51bWJlciBhbmQgc3RvcCB0aGUga2V5cHJlc3NcclxuXHRcdGlmICgoZS5zaGlmdEtleSB8fCAoZS5rZXlDb2RlIDwgNDggfHwgZS5rZXlDb2RlID4gNTcpKSAmJiAoZS5rZXlDb2RlIDwgOTYgfHwgZS5rZXlDb2RlID4gMTA1KSkge1xyXG5cdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XHJcblx0XHR9XHJcblx0fSk7XHJcblx0Ly8gb25seSBtdW1iZXJzIGlucHV0IGVuZFxyXG5cclxuXHQvLyBmaWxsdGVyIHByaWNlIHNsaWRlclxyXG5cdChmdW5jdGlvbiAoKSB7XHJcblxyXG5cdFx0dmFyIHNsaWRlck1pblJhbmdlID0gNTAwO1xyXG5cdFx0dmFyIHNsaWRlck1heFJhbmdlID0gMjAwMDA7XHJcblx0XHQkKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0JChcIiNzbGlkZXItcmFuZ2VcIikuc2xpZGVyKHtcclxuXHRcdFx0XHRyYW5nZTogdHJ1ZSxcclxuXHRcdFx0XHRtaW46IHNsaWRlck1pblJhbmdlLFxyXG5cdFx0XHRcdG1heDogc2xpZGVyTWF4UmFuZ2UsXHJcblx0XHRcdFx0dmFsdWVzOiBbc2xpZGVyTWluUmFuZ2UsIHNsaWRlck1heFJhbmdlXSxcclxuXHRcdFx0XHRzbGlkZTogZnVuY3Rpb24gKGV2ZW50LCB1aSkge1xyXG5cdFx0XHRcdFx0JChcIiNhbW91bnRfX21pblwiKS52YWwodWkudmFsdWVzWzBdKTtcclxuXHRcdFx0XHRcdCQoXCIjYW1vdW50X19tYXhcIikudmFsKHVpLnZhbHVlc1sxXSk7XHJcblx0XHRcdFx0XHQvL2NvbnNvbGUubG9nKHVpLnZhbHVlc1swXSk7XHJcblx0XHRcdFx0XHQvL2NvbnNvbGUubG9nKHVpLnZhbHVlc1sxXSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KTtcclxuXHJcblx0XHRcdCQoXCIjYW1vdW50X19taW4sICNhbW91bnRfX21heFwiKS5jaGFuZ2UoZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdHZhciB2YWx1ZU1pbiA9IHBhcnNlSW50KCQoXCIjYW1vdW50X19taW5cIikudmFsKCkpO1xyXG5cdFx0XHRcdHZhciB2YWx1ZU1heCA9IHBhcnNlSW50KCQoXCIjYW1vdW50X19tYXhcIikudmFsKCkpO1xyXG5cdFx0XHRcdC8vY29uc29sZS5sb2coXCJtaW4gXCIgKyB2YWx1ZU1pbik7XHJcblx0XHRcdFx0Ly9jb25zb2xlLmxvZyhcIm1heCBcIiArIHZhbHVlTWF4KTtcclxuXHRcdFx0XHRpZiAodmFsdWVNaW4gPiB2YWx1ZU1heCkge1xyXG5cdFx0XHRcdFx0dmFsdWVNaW4gPSB2YWx1ZU1heDtcclxuXHRcdFx0XHRcdC8vY29uc29sZS5sb2coXCJ2YWx1ZU1pbiA9IHZhbHVlTWF4XCIpXHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGlmICh2YWx1ZU1pbiA8IHNsaWRlck1pblJhbmdlKSB7XHJcblx0XHRcdFx0XHR2YWx1ZU1pbiA9IHNsaWRlck1pblJhbmdlO1xyXG5cdFx0XHRcdFx0Ly9jb25zb2xlLmxvZyhcInZhbHVlTWluID0gNTAwO1wiKVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRpZiAodmFsdWVNYXggPiBzbGlkZXJNYXhSYW5nZSkge1xyXG5cdFx0XHRcdFx0dmFsdWVNYXggPSBzbGlkZXJNYXhSYW5nZTtcclxuXHRcdFx0XHRcdC8vY29uc29sZS5sb2coXCJ2YWx1ZU1heCA9IDIwMDAwO1wiKVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHQvL2NvbnNvbGUubG9nKHZhbHVlTWluKTtcclxuXHRcdFx0XHQvL2NvbnNvbGUubG9nKHZhbHVlTWF4KTtcclxuXHRcdFx0XHQvL2NvbnNvbGUubG9nKFwic2xpZGVyIHZhbHVlcyBcIiArICQoIFwiI3NsaWRlci1yYW5nZVwiICkuc2xpZGVyKCBcIm9wdGlvblwiLCBcInZhbHVlc1wiICkpO1xyXG5cclxuXHRcdFx0XHQvLyBzZXQgc2xpZGVyIHZhbHVlcyA9IGlucHV0IHZhbHVlc1xyXG5cdFx0XHRcdCQoXCIjc2xpZGVyLXJhbmdlXCIpLnNsaWRlcihcIm9wdGlvblwiLCBcInZhbHVlc1wiLCBbdmFsdWVNaW4sIHZhbHVlTWF4XSk7XHJcblxyXG5cdFx0XHRcdC8vIHNldCBpbnB1dCB2YWx1ZXMgPSBzbGlkZXIgdmFsdWVzXHJcblxyXG5cdFx0XHRcdCQoXCIjYW1vdW50X19taW5cIikudmFsKHZhbHVlTWluKTtcclxuXHRcdFx0XHQkKFwiI2Ftb3VudF9fbWF4XCIpLnZhbCh2YWx1ZU1heCk7XHJcblx0XHRcdH0pO1xyXG5cclxuXHJcblx0XHR9KTtcclxuXHR9KSgpO1xyXG5cdC8vIGZpbGx0ZXIgcHJpY2Ugc2xpZGVyIGVuZCBcclxuXHJcblxyXG5cdC8vIGpzLXByb2R1Y3QtZ3JpZC1zbGlkZXJcclxuXHR2YXIgcHJvZHVjdEdyaWRTbGlkZXIgPSAkKCcuanMtcHJvZHVjdC1ncmlkLXNsaWRlcicpLnNsaWNrKHtcclxuXHRcdHNsaWRlc1RvU2hvdzogMyxcclxuXHRcdHNsaWRlc1RvU2Nyb2xsOiAxLFxyXG5cdFx0YXJyb3dzOiB0cnVlLFxyXG5cdFx0ZG90czogZmFsc2UsXHJcblx0XHRpbmZpbml0ZTogdHJ1ZSxcclxuXHRcdGxhenlMb2FkOiAnb25kZW1hbmQnLFxyXG5cdFx0bmV4dEFycm93OiAnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJwcm9kdWN0LWdyaWQtc2xpZGVyX19idG4tLW5leHQgcHJvZHVjdC1ncmlkLXNsaWRlcl9fYnRuXCI+JyArIGFycm93UmlnaHQgKyAnPC9idXR0b24+JyxcclxuXHRcdHByZXZBcnJvdzogJzxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwicHJvZHVjdC1ncmlkLXNsaWRlcl9fYnRuLS1wcmV2IHByb2R1Y3QtZ3JpZC1zbGlkZXJfX2J0blwiPicgKyBhcnJvd0xlZnQgKyAnPC9idXR0b24+JyxcclxuXHRcdHN3aXBlVG9TbGlkZTogdHJ1ZSxcclxuXHRcdHJlc3BvbnNpdmU6IFtcclxuXHRcdFx0e1xyXG5cdFx0XHRcdGJyZWFrcG9pbnQ6IDk5MixcclxuXHRcdFx0XHRzZXR0aW5nczoge1xyXG5cdFx0XHRcdFx0c2xpZGVzVG9TaG93OiAyXHJcblx0XHRcdFx0fVxyXG4gICAgfSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdGJyZWFrcG9pbnQ6IDc2OCxcclxuXHRcdFx0XHRzZXR0aW5nczoge1xyXG5cdFx0XHRcdFx0c2xpZGVzVG9TaG93OiAxXHJcblx0XHRcdFx0fVxyXG4gICAgfVxyXG4gIF1cclxuXHR9KTtcclxuXHQvLyBqcy1wcm9kdWN0LWdyaWQtc2xpZGVyIGVuZFxyXG5cclxuXHQvLyBqcy1pbWctc2xpZGVyXHJcblx0dmFyIGltZ1NsaWRlciA9ICQoJy5qcy1pbWctc2xpZGVyJykuc2xpY2soe1xyXG5cdFx0c2xpZGVzVG9TaG93OiAxLFxyXG5cdFx0c2xpZGVzVG9TY3JvbGw6IDEsXHJcblx0XHRhcnJvd3M6IHRydWUsXHJcblx0XHRkb3RzOiBmYWxzZSxcclxuXHRcdGluZmluaXRlOiB0cnVlLFxyXG5cdFx0bGF6eUxvYWQ6ICdvbmRlbWFuZCcsXHJcblx0XHRuZXh0QXJyb3c6ICc8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cInNsaWRlcl9fYnRuLS1uZXh0IHNsaWRlcl9fYnRuXCI+JyArIGFycm93UmlnaHQgKyAnPC9idXR0b24+JyxcclxuXHRcdHByZXZBcnJvdzogJzxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwic2xpZGVyX19idG4tLXByZXYgc2xpZGVyX19idG5cIj4nICsgYXJyb3dMZWZ0ICsgJzwvYnV0dG9uPicsXHJcblx0XHRzd2lwZVRvU2xpZGU6IHRydWVcclxuXHR9KTtcclxuXHQvLyBqcy1pbWctc2xpZGVyIGVuZFxyXG5cclxuXHQvLyBqcy1icmFuZC1zbGlkZXJcclxuXHR2YXIgYnJhbmRTbGlkZXIgPSAkKCcuanMtYnJhbmQtc2xpZGVyJyk7XHJcblxyXG5cdFx0XHRicmFuZFNsaWRlci5zbGljayh7XHJcblx0XHRcdFx0c2xpZGVzVG9TaG93OiA1LFxyXG5cdFx0XHRcdHNsaWRlc1RvU2Nyb2xsOiAxLFxyXG5cdFx0XHRcdHJvd3M6IDIsXHJcblx0XHRcdFx0YXJyb3dzOiB0cnVlLFxyXG5cdFx0XHRcdGRvdHM6IGZhbHNlLFxyXG5cdFx0XHRcdGluZmluaXRlOiB0cnVlLFxyXG5cdFx0XHRcdGxhenlMb2FkOiAnb25kZW1hbmQnLFxyXG5cdFx0XHRcdG5leHRBcnJvdzogJzxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwic2xpZGVyX19idG4tLW5leHQgc2xpZGVyX19idG5cIj4nICsgYXJyb3dSaWdodCArICc8L2J1dHRvbj4nLFxyXG5cdFx0XHRcdHByZXZBcnJvdzogJzxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwic2xpZGVyX19idG4tLXByZXYgc2xpZGVyX19idG5cIj4nICsgYXJyb3dMZWZ0ICsgJzwvYnV0dG9uPicsXHJcblx0XHRcdFx0c3dpcGVUb1NsaWRlOiB0cnVlLFxyXG5cdFx0XHRcdHJlc3BvbnNpdmU6IFtcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0YnJlYWtwb2ludDogOTkyLFxyXG5cdFx0XHRcdFx0XHRzZXR0aW5nczoge1xyXG5cdFx0XHRcdFx0XHRcdHNsaWRlc1RvU2hvdzogMlxyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHQgICAgfSxcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0YnJlYWtwb2ludDogNzY4LFxyXG5cdFx0XHRcdFx0XHRzZXR0aW5nczoge1xyXG5cdFx0XHRcdFx0XHRcdHNsaWRlc1RvU2hvdzogMVxyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHQgICAgfVxyXG5cdFx0ICBdXHJcblx0XHR9KTtcclxuXHJcblxyXG5cdFx0XHJcblx0Ly8ganMtYnJhbmQtc2xpZGVyIGVuZFxyXG5cdFxyXG5cdC8vIGpzLXByb2Qtc2xpZGVyXHJcblx0dmFyICRncmlkU2xpZGVycyA9ICQoXCIuanMtZ3JpZC1zbGlkZXJcIik7XHJcblxyXG5cdGlmKCRncmlkU2xpZGVycy5sZW5ndGgpIHtcclxuXHRcdCRncmlkU2xpZGVycy5lYWNoKGZ1bmN0aW9uIChpLCBlbCkge1xyXG5cdFx0XHR2YXIgJGdhbGxlcnlTbGlkZXIgPSAkKGVsKS5maW5kKFwiLmpzLXByb2QtZ2FsbGVyeS1zbGlkZXJcIik7XHJcblx0XHRcdHZhciAkaW1hZ2VzU2xpZGVyID0gJChlbCkuZmluZChcIi5qcy1wcm9kLWltYWdlLXNsaWRlclwiKTtcclxuXHRcdFx0dmFyICRnYWxsZXJ5U2xpZGVySW5kZXggPSAkZ2FsbGVyeVNsaWRlci5hZGRDbGFzcyhcImpzLXByb2QtZ2FsbGVyeS1zbGlkZXItLVwiICsgaSk7XHJcblx0XHRcdHZhciAkaW1hZ2VzU2xpZGVySW5kZXggPSAkaW1hZ2VzU2xpZGVyLmFkZENsYXNzKFwianMtcHJvZC1pbWFnZS1zbGlkZXItLVwiICsgaSk7XHJcblx0XHRcdC8vY29uc29sZS5sb2coJGdhbGxlcnlTbGlkZXJJbmRleCk7XHJcblx0XHRcdC8vY29uc29sZS5sb2coJGltYWdlc1NsaWRlckluZGV4KTtcclxuXHRcdFx0aW5pdEdyaWRTbGlkZXIoJGdhbGxlcnlTbGlkZXJJbmRleCwgJGltYWdlc1NsaWRlckluZGV4KTtcclxuXHRcdH0pOy8vIGVuZCAuZWFjaCgpXHJcblx0fVxyXG5cclxuXHRmdW5jdGlvbiBpbml0R3JpZFNsaWRlcigkZ2FsbGVyeVNsaWRlckluZGV4LCAkaW1hZ2VzU2xpZGVySW5kZXgpIHtcclxuXHRcdHZhciAkZ2FsbGVyeVNsaWRlciA9ICRnYWxsZXJ5U2xpZGVySW5kZXg7XHJcblx0XHQkZ2FsbGVyeVNsaWRlci5zbGljayh7XHJcblx0XHRcdHNsaWRlc1RvU2hvdzogNCxcclxuXHRcdFx0c2xpZGVzVG9TY3JvbGw6IDEsXHJcblx0XHRcdGFycm93czogZmFsc2UsXHJcblx0XHRcdGRvdHM6IGZhbHNlLFxyXG5cdFx0XHRsYXp5TG9hZDogJ29uZGVtYW5kJyxcclxuXHRcdFx0dmVydGljYWw6IHRydWUsXHJcblx0XHRcdGluZmluaXRlOiB0cnVlLFxyXG5cdFx0XHR2ZXJ0aWNhbFN3aXBpbmc6IHRydWUsXHJcblx0XHRcdHN3aXBlVG9TbGlkZTogdHJ1ZSxcclxuXHRcdFx0Zm9jdXNPblNlbGVjdDogdHJ1ZSxcclxuXHRcdFx0YXNOYXZGb3I6ICRpbWFnZXNTbGlkZXJJbmRleCxcclxuXHRcdFx0cmVzcG9uc2l2ZTogW1xyXG5cclxuXHRcdFx0XHR7XHJcblx0XHRcdFx0XHRicmVha3BvaW50OiA3NjgsXHJcblx0XHRcdFx0XHRzZXR0aW5nczoge1xyXG5cdFx0XHRcdFx0XHR2ZXJ0aWNhbDogZmFsc2VcclxuXHRcdFx0XHRcdH1cclxuICAgIH1cclxuICBdXHJcblx0XHR9KTtcclxuXHRcdHZhciAkaW1hZ2VzU2xpZGVyID0gJGltYWdlc1NsaWRlckluZGV4O1xyXG5cdFx0JGltYWdlc1NsaWRlci5zbGljayh7XHJcblx0XHRcdHNsaWRlc1RvU2hvdzogMSxcclxuXHRcdFx0c2xpZGVzVG9TY3JvbGw6IDEsXHJcblx0XHRcdGFycm93czogZmFsc2UsXHJcblx0XHRcdGRvdHM6IGZhbHNlLFxyXG5cdFx0XHRmYWRlOiB0cnVlLFxyXG5cdFx0XHRpbmZpbml0ZTogdHJ1ZSxcclxuXHRcdFx0bGF6eUxvYWQ6ICdvbmRlbWFuZCcsXHJcblx0XHRcdGFzTmF2Rm9yOiAkZ2FsbGVyeVNsaWRlckluZGV4LFxyXG5cdFx0XHRzd2lwZTogdHJ1ZSxcclxuXHRcdFx0c3dpcGVUb1NsaWRlOiBmYWxzZVxyXG5cdFx0fSk7XHJcblx0XHQvL2NvbnNvbGUubG9nKFwiaW5pdFByb2RTbGlkZXIoKVwiKTtcclxuXHR9XHJcblx0Ly8ganMtcHJvZC1zbGlkZXIgZW5kXHJcblxyXG5cdC8vIGpxdWVyeSB1aSB0YWJzXHJcblx0JChcIi5qcy11aS10YWJzXCIpLnRhYnMoe1xyXG5cdFx0YmVmb3JlQWN0aXZhdGU6IGZ1bmN0aW9uIChldmVudCwgdWkpIHtcclxuXHRcdFx0cHJvZHVjdEdyaWRTbGlkZXIuc2xpY2soXCJzbGlja0dvVG9cIiwgMCk7IC8vIGZpeCBmb3Igc2xpY2sgc2xpZGVyXHJcblx0XHR9XHJcblx0fSk7XHJcblx0Ly8ganF1ZXJ5IHVpIHRhYnMgZW5kXHJcblxyXG5cdGZ1bmN0aW9uIGluaXRGb3JtcygpIHtcclxuXHRcdC8vIHRzci1zZWxlY3RcclxuXHRcdHZhciBzZWxlY3QgPSAkKFwiLnRzci1zZWxlY3RcIik7XHJcblx0XHRzZWxlY3Quc3R5bGVyKCk7XHJcblx0XHQvLyB0c3Itc2VsZWN0IGVuZFxyXG5cclxuXHRcdC8vIHRzci1xdWFudGl0eS1mb3JtcyBpbmNyZW1lbnRcclxuXHRcdGZ1bmN0aW9uIGluaXRRdWFudGl0eUZvcm0oKSB7XHJcblx0XHRcdHZhciBpbnB1dCA9ICQoXCIudHNyLXF1YW50aXR5LWZvcm1zID4gaW5wdXRcIik7XHJcblxyXG5cdFx0XHRpZiAoaW5wdXQuaGFzQ2xhc3MoXCJxdWFudGl0eS1mb3Jtcy1pbml0XCIpKSB7XHJcblx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdCQoXCIudHNyLXF1YW50aXR5LWZvcm1zX19pbmMsIC50c3ItcXVhbnRpdHktZm9ybXNfX2RlY1wiKS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRcdHZhciAkYnV0dG9uID0gJCh0aGlzKTtcclxuXHRcdFx0XHRcdHZhciBvbGRWYWx1ZSA9ICRidXR0b24ucGFyZW50KCkuZmluZChcImlucHV0XCIpLnZhbCgpO1xyXG5cdFx0XHRcdFx0aWYgKCRidXR0b24uaGFzQ2xhc3MoXCJ0c3ItcXVhbnRpdHktZm9ybXNfX2luY1wiKSkge1xyXG5cdFx0XHRcdFx0XHR2YXIgbmV3VmFsID0gcGFyc2VGbG9hdChvbGRWYWx1ZSkgKyAxO1xyXG5cdFx0XHRcdFx0fSBlbHNlIGlmICgkYnV0dG9uLmhhc0NsYXNzKFwidHNyLXF1YW50aXR5LWZvcm1zX19kZWNcIikpIHtcclxuXHRcdFx0XHRcdFx0aWYgKG9sZFZhbHVlID4gMSkge1xyXG5cdFx0XHRcdFx0XHRcdHZhciBuZXdWYWwgPSBwYXJzZUZsb2F0KG9sZFZhbHVlKSAtIDE7XHJcblx0XHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdFx0bmV3VmFsID0gMTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0JGJ1dHRvbi5wYXJlbnQoKS5maW5kKFwiaW5wdXRcIikudmFsKG5ld1ZhbCkudHJpZ2dlcihcImNoYW5nZVwiKTtcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0XHRpbnB1dC5hZGRDbGFzcyhcInF1YW50aXR5LWZvcm1zLWluaXRcIik7XHJcblx0XHRcdH1cclxuXHJcblx0XHR9XHJcblx0XHRpbml0UXVhbnRpdHlGb3JtKCk7XHJcblx0XHQvLyB0c3ItcXVhbnRpdHktZm9ybXMgZW5kXHRcclxuXHJcblx0XHQvLyB0c3ItcmF0aW5nLWlucHV0XHJcblx0XHQkKCcuanMtcmF0aW5nLWlucHV0JykucmF0aW5nKCk7XHJcblx0XHQvLyB0c3ItcmF0aW5nLWlucHV0IGVuZFxyXG5cdH1cclxuXHJcblx0aW5pdEZvcm1zKCk7XHJcblxyXG5cclxuXHJcblxyXG5cdC8vIGZpbGUgaW5wdXQgY29udHJvbGxlclxyXG5cdChmdW5jdGlvbiAoKSB7XHJcblx0XHQvL3ZhciBmaWxlX2FwaSA9ICggd2luZG93LkZpbGUgJiYgd2luZG93LkZpbGVSZWFkZXIgJiYgd2luZG93LkZpbGVMaXN0ICYmIHdpbmRvdy5CbG9iICkgPyB0cnVlIDogZmFsc2U7XHJcblx0XHR2YXIgZmlsZUlucHV0ID0gJCgnI2ZpbGVfYnRuJyk7XHJcblx0XHR2YXIgZmlsZUlucHV0TGFiZWwgPSAkKCcuZmlsZS1pbnB1dF9fbGFiZWwnKTtcclxuXHJcblx0XHR2YXIgb3BlbkZpbGUgPSBmdW5jdGlvbiAoZXZlbnQpIHtcclxuXHRcdFx0dmFyIGlucHV0ID0gZXZlbnQudGFyZ2V0O1xyXG5cdFx0XHRpZiAoaW5wdXQuZmlsZXNbMF0udHlwZS5tYXRjaCgnaW1hZ2UuKicpKSB7XHJcblx0XHRcdFx0Ly9jb25zb2xlLmxvZyhcImltZ1wiKTtcclxuXHRcdFx0XHR2YXIgcmVhZGVyID0gbmV3IEZpbGVSZWFkZXIoKTtcclxuXHRcdFx0XHR2YXIgZGF0YVVSTDtcclxuXHRcdFx0XHRyZWFkZXIub25sb2FkID0gZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdFx0ZGF0YVVSTCA9IHJlYWRlci5yZXN1bHQ7XHJcblx0XHRcdFx0XHRmaWxlSW5wdXRMYWJlbC5jc3MoXCJiYWNrZ3JvdW5kLWltYWdlXCIsIFwidXJsKFwiICsgZGF0YVVSTCArIFwiKVwiKTtcclxuXHRcdFx0XHR9O1xyXG5cdFx0XHRcdHJlYWRlci5yZWFkQXNEYXRhVVJMKGlucHV0LmZpbGVzWzBdKTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRhbGVydChcItCc0L7QttC90L4g0LfQsNCz0YDRg9C20LDRgtGMINGC0L7Qu9GM0LrQviDRhNC+0YLQvlwiKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdH07XHJcblx0XHRmaWxlSW5wdXQub24oXCJjaGFuZ2VcIiwgZnVuY3Rpb24gKGUpIHtcclxuXHRcdFx0b3BlbkZpbGUoZSk7XHJcblx0XHR9KTtcclxuXHR9KCkpO1xyXG5cdC8vIGZpbGUgaW5wdXQgY29udHJvbGxlciBlbmRcclxuXHJcblx0Ly8gcG9wLXVwIGluaXRcclxuXHQkKFwiLmpzLXBvcHVwXCIpLm1hZ25pZmljUG9wdXAoe1xyXG5cdFx0dHlwZTogJ2lubGluZScsXHJcblx0XHRwcmVsb2FkZXI6IGZhbHNlLFxyXG5cdFx0Zm9jdXM6IGZhbHNlXHJcblx0fSk7XHJcblxyXG5cdCQoJy5qcy1wb3B1cC12aWRlbycpLm1hZ25pZmljUG9wdXAoe1xyXG5cdFx0ZGlzYWJsZU9uOiA3MDAsXHJcblx0XHR0eXBlOiAnaWZyYW1lJyxcclxuXHRcdG1haW5DbGFzczogJ21mcC1mYWRlJyxcclxuXHRcdHJlbW92YWxEZWxheTogMTYwLFxyXG5cdFx0cHJlbG9hZGVyOiBmYWxzZSxcclxuXHJcblx0XHRmaXhlZENvbnRlbnRQb3M6IGZhbHNlXHJcblx0fSk7XHJcblx0JChcIi5qcy1mYXN0LXZpZXdcIikubWFnbmlmaWNQb3B1cCh7XHJcblx0XHR0eXBlOiAnYWpheCcsXHJcblx0XHRwcmVsb2FkZXI6IHRydWUsXHJcblx0XHRmb2N1czogZmFsc2UsXHJcblx0XHRzaG93Q2xvc2VCdG46IGZhbHNlLFxyXG5cdFx0Y2FsbGJhY2tzOiB7XHJcblx0XHRcdGFqYXhDb250ZW50QWRkZWQ6IGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRpbml0UHJvZFNsaWRlcigpO1xyXG5cdFx0XHRcdGluaXRGb3JtcygpO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fSk7XHJcblx0XHJcblx0JChcIi5qcy1wb3AtdXAtYWpheFwiKS5tYWduaWZpY1BvcHVwKHtcclxuXHRcdHR5cGU6ICdhamF4JyxcclxuXHRcdHByZWxvYWRlcjogdHJ1ZSxcclxuXHRcdGZvY3VzOiBmYWxzZSxcclxuXHRcdHNob3dDbG9zZUJ0bjogZmFsc2UsXHJcblx0XHRjYWxsYmFja3M6IHtcclxuXHRcdFx0YWpheENvbnRlbnRBZGRlZDogZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdGluaXRGb3JtcygpO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fSk7XHJcblx0Ly8gcG9wLXVwIGluaXQgZW5kXHJcblxyXG5cdC8vIGZpbHRlciBzaG93IHNjcmlwdFxyXG5cdHZhciBmaWx0ZXJCdG4gPSAkKFwiLmpzLWZpbHRlcl9fdGl0bGVcIik7XHJcblx0dmFyIGZpbHRlckJvZHkgPSAkKFwiLmpzLWZpbHRlcl9faW5uZXJcIik7XHJcblx0ZmlsdGVyQnRuLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xyXG5cdFx0ZmlsdGVyQm9keS5zbGlkZVRvZ2dsZSgpO1xyXG5cdH0pO1xyXG5cdCQod2luZG93KS5yZXNpemUoZnVuY3Rpb24gKCkge1xyXG5cdFx0dmFyIG1xbF85OTIgPSB3aW5kb3cubWF0Y2hNZWRpYShcIihtYXgtd2lkdGg6IDk5MnB4KVwiKS5tYXRjaGVzO1xyXG5cdFx0Ly9jb25zb2xlLmxvZyhtcWxfNzY4KTtcclxuXHRcdGlmICghbXFsXzk5Mikge1xyXG5cdFx0XHRmaWx0ZXJCb2R5LmF0dHIoXCJzdHlsZVwiLCBcIlwiKTtcclxuXHRcdH1cclxuXHR9KTtcclxuXHQvLyBmaWx0ZXIgc2hvdyBzY3JpcHQgZW5kXHJcblxyXG5cclxuXHJcbn0pOyAvLyByZWFkeVxyXG5cclxuLy8gcmVnaXN0cmF0aW9uLXN1Y2Nlc3NcclxuZnVuY3Rpb24gc2hvd1JlZ1N1Y2Nlc3MoKSB7XHJcblx0JC5tYWduaWZpY1BvcHVwLm9wZW4oe1xyXG5cdFx0aXRlbXM6IHtcclxuXHRcdFx0c3JjOiAnI3JlZ2lzdHJhdGlvbi1zdWNjZXNzJ1xyXG5cdFx0fSxcclxuXHRcdHR5cGU6ICdpbmxpbmUnLFxyXG5cdFx0cHJlbG9hZGVyOiBmYWxzZSxcclxuXHRcdGZvY3VzOiBmYWxzZVxyXG5cdH0pO1xyXG59XHJcbi8vIHJlZ2lzdHJhdGlvbi1zdWNjZXNzIGVuZFxyXG5cclxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xyXG5cdHZhciBhcnJvd0xlZnQgPSAnPGkgY2xhc3M9XCJpY29uLWFycm93X2xlZnRcIj48L2k+JztcclxuXHR2YXIgYXJyb3dSaWdodCA9ICc8aSBjbGFzcz1cImljb24tYXJyb3dfcmlnaHRcIj48L2k+JztcclxuXHJcblx0dmFyXHQkb3VyQnJlbmRzU2xpZGVyVmlkZW8gPSAkKFwiLmpzLW91ci1icmVuZHMtc2xpZGVyLXZpZGVvXCIpLFxyXG5cdFx0XHRvdXJTbGlkZVZpZGVvID0gJG91ckJyZW5kc1NsaWRlclZpZGVvLmZpbmQoXCIuYmFubmVyXCIpLFxyXG5cdFx0XHRvdXJCcmVuZHNTbGlkZXJIYWxmID0gXCJvdXItYnJlbmRzLXNsaWRlclwiO1xyXG5cclxuXHRmdW5jdGlvbiBzdHlsZUJhbm5lciAoKSB7XHJcblx0XHQkb3VyQnJlbmRzU2xpZGVyVmlkZW8uYWRkQ2xhc3Mob3VyQnJlbmRzU2xpZGVySGFsZik7XHJcblx0fVxyXG5cclxuXHRmdW5jdGlvbiBpbml0U2xpZGVyICgpIHtcclxuXHRcdCRvdXJCcmVuZHNTbGlkZXJWaWRlby5zbGljayh7XHJcblx0XHRcdHNsaWRlc1RvU2hvdzogMSxcclxuXHRcdFx0c2xpZGVzVG9TY3JvbGw6IDEsXHJcblx0XHRcdGFycm93czogdHJ1ZSxcclxuXHRcdFx0aW5maW5pdGU6IHRydWUsXHJcblx0XHRcdGNlbnRlck1vZGU6IHRydWUsXHJcblx0XHRcdGNlbnRlclBhZGRpbmc6ICcyOTBweCcsXHJcblx0XHRcdC8vdmFyaWFibGVXaWR0aDogdHJ1ZSxcclxuXHRcdFx0bGF6eUxvYWQ6ICdvbmRlbWFuZCcsXHJcblx0XHRcdG5leHRBcnJvdzogJzxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYmFubmVyLXNsaWRlcl9fYnRuLS1uZXh0IGJhbm5lci1zbGlkZXJfX2J0blwiPiAnICsgYXJyb3dSaWdodCArICcgPC9idXR0b24+JyxcclxuXHRcdFx0cHJldkFycm93OiAnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJiYW5uZXItc2xpZGVyX19idG4tLXByZXYgYmFubmVyLXNsaWRlcl9fYnRuXCI+JyArIGFycm93TGVmdCArICc8L2J1dHRvbj4nLFxyXG5cdFx0XHRzd2lwZVRvU2xpZGU6IHRydWUsXHJcblx0XHRcdHJlc3BvbnNpdmU6IFtcclxuXHRcdCAgICB7XHJcblx0XHQgICAgICBicmVha3BvaW50OiAxNjAwLFxyXG5cdFx0ICAgICAgc2V0dGluZ3M6IHtcclxuXHRcdCAgICAgIFx0Y2VudGVyUGFkZGluZzogXCIxMDBweFwiXHJcblx0XHQgICAgICB9XHJcblx0XHQgICAgfSxcclxuXHRcdCAgICB7XHJcblx0XHQgICAgICBicmVha3BvaW50OiAxMDI0LFxyXG5cdFx0ICAgICAgc2V0dGluZ3M6IHtcclxuXHRcdCAgICAgIFx0c2xpZGVzVG9TaG93OiAxLFxyXG5cdFx0XHRcdFx0XHRzbGlkZXNUb1Njcm9sbDogMSxcclxuXHRcdFx0XHRcdFx0Y2VudGVyUGFkZGluZzogXCIxMDBweFwiXHJcblx0XHQgICAgICB9XHJcblx0XHQgICAgfSxcclxuXHRcdCAgICB7XHJcblx0XHQgICAgICBicmVha3BvaW50OiA3NjgsXHJcblx0XHQgICAgICBzZXR0aW5nczoge1xyXG5cdFx0XHRcdFx0XHRzbGlkZXNUb1Nob3c6IDEsXHJcblx0XHRcdFx0XHRcdHNsaWRlc1RvU2Nyb2xsOiAxLFxyXG5cdFx0XHRcdFx0XHRjZW50ZXJQYWRkaW5nOiBcIjUwcHhcIlxyXG5cdFx0ICAgICAgfVxyXG5cdFx0ICAgIH1cclxuXHRcdCAgXVxyXG5cdFx0fSk7XHJcblxyXG5cdCQod2luZG93KS5yZXNpemUoZnVuY3Rpb24gKCkge1xyXG5cdFx0JG91ckJyZW5kc1NsaWRlclZpZGVvLnNsaWNrKCdzbGlja0dvVG8nLCAwKTtcclxuXHR9KTtcclxuXHJcblx0XHQkb3VyQnJlbmRzU2xpZGVyVmlkZW8uc2xpY2soJ3NsaWNrR29UbycsIDApO1xyXG5cdH1cclxuXHJcblxyXG5cdGZ1bmN0aW9uIGluaXRWaWRlbyAoKSB7XHJcblx0XHQkb3VyQnJlbmRzU2xpZGVyVmlkZW8ucmVtb3ZlQ2xhc3MoJ291ci1icmVuZHMtc2xpZGVyLWZ1bGwnKS5yZW1vdmVDbGFzcyhvdXJCcmVuZHNTbGlkZXJIYWxmKTtcclxuXHRcdGlmKG91clNsaWRlVmlkZW8ubGVuZ3RoID09PSAxKSB7XHJcblx0XHRcdCRvdXJCcmVuZHNTbGlkZXJWaWRlby5hZGRDbGFzcyhcIm91ci1icmVuZHMtc2xpZGVyLWZ1bGxcIik7XHJcblx0XHR9IGVsc2UgaWYgKG91clNsaWRlVmlkZW8ubGVuZ3RoID09PSAyKSB7XHJcblx0XHRcdHN0eWxlQmFubmVyICgpO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0aW5pdFNsaWRlciAoKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdHZhciAkb3VyQnJlbmRzU2xpZGVyID0gJChcIi5qcy1vdXItYnJhbmRzLXNsaWRlclwiKSxcclxuXHRcdFx0JG91ckJyZW5kc1NsaWRlclRlc3QgPSAkKFwiLmpzLW91ci1icmFuZHMtc2xpZGVyLXRlc3RcIiksXHJcblx0XHRcdCRvdXJCcmVuZHNTbGlkZXJTbGlkZSA9ICRvdXJCcmVuZHNTbGlkZXIuZmluZChcIi5iYW5uZXJcIik7XHJcblx0XHRcdHZhciBzbGlkZVRvU2hvdyA9IDI7XHJcblxyXG5cdFx0aWYoJG91ckJyZW5kc1NsaWRlclNsaWRlLmxlbmd0aCA9PT0gMikge1xyXG5cdFx0XHRzbGlkZVRvU2hvdyA9IDE7XHJcblx0XHRcdGluaXRPdXJCcmFuZHNTbGlkZXIoKTtcclxuXHRcdH1cclxuXHJcblx0XHRpZigkb3VyQnJlbmRzU2xpZGVyU2xpZGUubGVuZ3RoID4gMikge1xyXG5cdFx0XHRpbml0T3VyQnJhbmRzU2xpZGVyKCk7XHJcblx0XHR9XHJcblxyXG5cdGZ1bmN0aW9uIGluaXRPdXJCcmFuZHNTbGlkZXIgKCkge1xyXG5cdFx0JG91ckJyZW5kc1NsaWRlci5zbGljayh7XHJcblx0XHQgIGluZmluaXRlOiB0cnVlLFxyXG5cdFx0ICBzcGVlZDogMzAwLFxyXG5cdFx0ICBzbGlkZXNUb1Nob3c6IHNsaWRlVG9TaG93LFxyXG5cdFx0ICBzbGlkZXNUb1Njcm9sbDogMSxcclxuXHRcdCAgY2VudGVyTW9kZTogdHJ1ZSxcclxuXHRcdCAgY2VudGVyUGFkZGluZzogJzE1MHB4JyxcclxuXHQgICAgbmV4dEFycm93OiAnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJiYW5uZXItc2xpZGVyX19idG4tLW5leHQgYmFubmVyLXNsaWRlcl9fYnRuXCI+ICcgKyBhcnJvd1JpZ2h0ICsgJyA8L2J1dHRvbj4nLFxyXG5cdFx0XHRwcmV2QXJyb3c6ICc8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJhbm5lci1zbGlkZXJfX2J0bi0tcHJldiBiYW5uZXItc2xpZGVyX19idG5cIj4nICsgYXJyb3dMZWZ0ICsgJzwvYnV0dG9uPicsXHJcblx0ICAgIHZhcmlhYmxlV2lkdGg6IHRydWUsXHJcblx0ICAgIHJlc3BvbnNpdmU6IFt7XHJcblx0ICAgICAgICBicmVha3BvaW50OiA5OTIsXHJcblx0ICAgICAgICBzZXR0aW5nczoge1xyXG5cdCAgICAgICAgXHRjZW50ZXJQYWRkaW5nOiAnNTBweCcsXHJcblx0ICAgICAgICB9XHJcblx0ICAgIH0se1xyXG5cdCAgICAgICAgYnJlYWtwb2ludDogNzY4LFxyXG5cdCAgICAgICAgc2V0dGluZ3M6IHtcclxuXHQgICAgICAgICAgICBzbGlkZXNUb1Nob3c6IDEsXHJcblx0ICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXHJcblx0ICAgICAgICAgICAgY2VudGVyUGFkZGluZzogJzBweCcsXHJcblx0ICAgICAgICB9XHJcblx0ICAgIH1dXHJcblx0fSk7XHJcblxyXG5cdFx0JCh3aW5kb3cpLnJlc2l6ZShmdW5jdGlvbiAoKSB7XHJcblx0XHRcdCRvdXJCcmVuZHNTbGlkZXIuc2xpY2soJ3NsaWNrR29UbycsIDApO1xyXG5cdFx0fSk7XHJcblxyXG5cdFx0JG91ckJyZW5kc1NsaWRlci5zbGljaygnc2xpY2tHb1RvJywgMCk7XHJcblx0fVxyXG5cclxuXHRcdFxyXG5cclxuXHQkb3VyQnJlbmRzU2xpZGVyVGVzdC5zbGljayh7XHJcblx0ICBpbmZpbml0ZTogdHJ1ZSxcclxuXHQgIHNwZWVkOiAzMDAsXHJcblx0ICBzbGlkZXNUb1Nob3c6IDIsXHJcblx0ICBzbGlkZXNUb1Njcm9sbDogMSxcclxuXHQgIGNlbnRlck1vZGU6IHRydWUsXHJcblx0ICBjZW50ZXJQYWRkaW5nOiAnMTUwcHgnLFxyXG4gICAgbmV4dEFycm93OiAnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJiYW5uZXItc2xpZGVyX19idG4tLW5leHQgYmFubmVyLXNsaWRlcl9fYnRuXCI+ICcgKyBhcnJvd1JpZ2h0ICsgJyA8L2J1dHRvbj4nLFxyXG5cdFx0cHJldkFycm93OiAnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJiYW5uZXItc2xpZGVyX19idG4tLXByZXYgYmFubmVyLXNsaWRlcl9fYnRuXCI+JyArIGFycm93TGVmdCArICc8L2J1dHRvbj4nLFxyXG4gICAgLy92YXJpYWJsZVdpZHRoOiB0cnVlLFxyXG4gICAgIHJlc3BvbnNpdmU6IFt7XHJcblx0ICAgICAgICBicmVha3BvaW50OiA5OTIsXHJcblx0ICAgICAgICBzZXR0aW5nczoge1xyXG5cdCAgICAgICAgXHRjZW50ZXJQYWRkaW5nOiAnMTAwcHgnLFxyXG5cdCAgICAgICAgfVxyXG5cdCAgICB9LHtcclxuXHQgICAgICAgIGJyZWFrcG9pbnQ6IDc2OCxcclxuXHQgICAgICAgIHNldHRpbmdzOiB7XHJcblx0ICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAxLFxyXG5cdCAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxyXG5cdCAgICAgICAgICAgIGNlbnRlclBhZGRpbmc6ICc1MHB4JyxcclxuXHQgICAgICAgIH1cclxuXHQgICAgfV1cclxuXHR9KTtcclxuXHJcblx0XHQkKHdpbmRvdykucmVzaXplKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0JG91ckJyZW5kc1NsaWRlclRlc3Quc2xpY2soJ3NsaWNrR29UbycsIDApO1xyXG5cdFx0fSk7XHJcblxyXG5cdFx0JG91ckJyZW5kc1NsaWRlclRlc3Quc2xpY2soJ3NsaWNrR29UbycsIDApO1xyXG5cclxuXHRpbml0VmlkZW8gKCk7XHJcblxyXG5cclxufSk7XHJcbi8vb3VyLWJyZW5kcy1zbGlkZXIgXHJcbiIsIiQoZnVuY3Rpb24gKCkge1xyXG5cdC8vIHN3aXBlIG1lbnVcclxuXHQoZnVuY3Rpb24gKCkge1xyXG5cdFx0dmFyICR0b3VjaCA9ICQoJy5qcy10b2dnbGUtbWVudScpO1xyXG5cdFx0dmFyICR0b3VjaF9hbmltYXRlID0gJCgnLmpzLXRvZ2dsZS1tZW51IC5zYW5kd2ljaCcpO1xyXG5cdFx0dmFyIGNsYXNzTmFtZSA9IFwiYWN0aXZlXCI7XHJcblx0XHR2YXIgJG1lbnUgPSAkKCcuc3dpcGUtbWVudScpO1xyXG5cclxuXHRcdGZ1bmN0aW9uIHNob3dNZW51KCkge1xyXG5cdFx0XHQkKFwiYm9keVwiKS5hZGRDbGFzcyhjbGFzc05hbWUpO1xyXG5cdFx0XHQkdG91Y2hfYW5pbWF0ZS5hZGRDbGFzcyhjbGFzc05hbWUpO1xyXG5cdFx0XHQvL2NvbnNvbGUubG9nKFwic2hvd1wiKTtcclxuXHRcdH1cclxuXHJcblx0XHRmdW5jdGlvbiBoaWRlTWVudSgpIHtcclxuXHRcdFx0JChcImJvZHlcIikucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lKTtcclxuXHRcdFx0JHRvdWNoX2FuaW1hdGUucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lKTtcclxuXHRcdFx0Ly9jb25zb2xlLmxvZyhcImhpZGVcIik7XHJcblx0XHR9XHJcblxyXG5cdFx0JHRvdWNoLm9uKCdjbGljaycsIGZ1bmN0aW9uIChlKSB7XHJcblx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcclxuXHRcdFx0ZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuXHRcdFx0Ly9jb25zb2xlLmxvZyhcImNsaWNrXCIpO1xyXG5cdFx0XHQvL2NvbnNvbGUubG9nKCEoJHRvdWNoX2FuaW1hdGUuaGFzQ2xhc3MoY2xhc3NOYW1lKSkpO1xyXG5cdFx0XHRpZiAoISgkdG91Y2hfYW5pbWF0ZS5oYXNDbGFzcyhjbGFzc05hbWUpKSkge1xyXG5cdFx0XHRcdHNob3dNZW51KCk7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0aGlkZU1lbnUoKTtcclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblx0XHQkKFwiLndyYXBwZXJcIikub24oJ2NsaWNrJywgZnVuY3Rpb24gKGUpIHtcclxuXHRcdFx0Ly9lLnByZXZlbnREZWZhdWx0KCk7XHJcblx0XHRcdGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcblx0XHRcdGhpZGVNZW51KCk7XHJcblx0XHR9KTtcclxuXHRcdCRtZW51Lm9uKCdjbGljaycsIGZ1bmN0aW9uIChlKSB7XHJcblx0XHRcdGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcblx0XHR9KTtcclxuXHRcdCQod2luZG93KS5yZXNpemUoZnVuY3Rpb24gKCkge1xyXG5cdFx0XHR2YXIgd2lkID0gJCh3aW5kb3cpLndpZHRoKCk7XHJcblx0XHRcdGlmICh3aWQgPiA3NjcpIHtcclxuXHRcdFx0XHRoaWRlTWVudSgpO1xyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHR9KSgpO1xyXG5cdC8vIHN3aXBlIG1lbnUgZW5kXHJcbn0pOyJdfQ==
